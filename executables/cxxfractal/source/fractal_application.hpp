/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief The main windows application
 *
 * @author Frans Labuschagne
 * @date   2020-01-13
 *
 * @details
 * This file contains a class that create the main windows application.
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef h1c64c4fa_6362_42b4_96bb_bc0d6bbd2402
#define h1c64c4fa_6362_42b4_96bb_bc0d6bbd2402

#include "cxxwin/application.hpp"
#include "fractal_interface.hpp"

#include <memory>

namespace cxxfractal
{

class FractalApplication : public cxxwin::Application
{
public:
	FractalApplication(int bmpWidth, int bmpHeight);

	virtual ~FractalApplication();

	bool HandleDestroy();

	bool HandleDisplayChange(int depth, int width, int height);

	bool HandleResize(cxxwin::ResizeTypes how, int width, int height);

	bool HandlePaint();

	bool HandleKey(cxxwin::KeyDirections       direction,
	               unsigned                    virtualCode,
	               unsigned                    scanCode,
	               unsigned                    repeatCount,
	               cxxwin::KeyStateFlags::type flags);

    void SetFractal(std::unique_ptr<FractalInterface>& fractal)
    {
        fractal_.swap(fractal);
    }

private:
	virtual HRESULT CreateDeviceResources() override;
	virtual void DiscardDeviceResources() override;

	HRESULT CreateBitmap(int width, int height);
   HRESULT UpdateBitmap();

	int                       bmpWidth_           = 640;
	int                       bmpHeight_          = 480;
	ID2D1Bitmap *             bitmap_             = nullptr;
	IDWriteTextFormat *       writeTextFormat_    = nullptr;

	std::wstring          text_;
	ID2D1SolidColorBrush* txtBrush_ = nullptr;

    std::unique_ptr<FractalInterface> fractal_{nullptr};

    uint64_t frequency_;
};

} /* namespace cxxfractal */

#endif /* h1c64c4fa_6362_42b4_96bb_bc0d6bbd2402 */
