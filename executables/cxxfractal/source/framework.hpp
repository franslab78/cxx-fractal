/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief Framework
 *
 * @author Frans Labuschagne
 * @date   2020-01-13
 *
 * @details
 * Windows framework
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef HEFB5DF68_DA2F_4ED3_A73A_EAA76B7750DC
#define HEFB5DF68_DA2F_4ED3_A73A_EAA76B7750DC

#define WIN32_LEAN_AND_MEAN    // Exclude rarely-used stuff from Windows headers

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0601 /* WIN7 */
//#define _WIN32_WINNT 0x0A00 /* WIN10 */
#endif

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif

#include <SDKDDKVer.h>

// Windows Header Files
#include <windows.h>
// C RunTime Header Files

// DirectX headers
#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

// Standard
#include <cstdint>

template<class Interface>
inline void SafeRelease(
	Interface **ppInterfaceToRelease
)
{
	if (*ppInterfaceToRelease != NULL)
	{
		(*ppInterfaceToRelease)->Release();

		(*ppInterfaceToRelease) = NULL;
	}
}


#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif

#endif /* HEFB5DF68_DA2F_4ED3_A73A_EAA76B7750DC */
