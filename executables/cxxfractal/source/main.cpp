/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief cxx-fractal application
 *
 * @author Frans Labuschagne
 * @date   2020-02-12
 *
 * @details
 *    cxx-fractal application
 */
/*----------------------------------------------------------------------------*/

#include "fractal_application.hpp"
#include "mandelbrot.hpp"

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

#include "clipp.h"

#include <iostream>

/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	using namespace clipp;

	(void)argc; /* Unused parameter */
	(void)argv; /* Unused parameter */

#ifndef NDEBUG
	// Use HeapSetInformation to specify that the process should
	// terminate if the heap manager detects an error in any heap used
	// by the process.
	// The return value is ignored, because we want to continue running in the
	// unlikely event that HeapSetInformation fails.
	HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);
#endif
	int bmpWidth = 640;
	int bmpHeight = 480;
	auto cli = (
        option("--bmp-width") & value("bimap width", bmpWidth).doc("Bitmap width in pixels"),
		option("--bmp-height") & value("bimap height", bmpHeight).doc("Bitmap height in pixels")
    );

    if(!parse(argc, argv, cli)) std::cout << make_man_page(cli, argv[0]);

	cxxfractal::FractalApplication app(bmpWidth, bmpHeight);
    std::unique_ptr<cxxfractal::FractalInterface> fractal(new cxxfractal::Mandelbrot);
    app.SetFractal(fractal);
	return app.Execute();
}
