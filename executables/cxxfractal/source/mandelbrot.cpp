/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief The Mandelbrot 
 *
 * @author Frans Labuschagne
 * @date   2020-02-13
 *
 * @details
 * This file contains a Mandelbrot.
 */
/*----------------------------------------------------------------------------*/

#include "mandelbrot.hpp"
#include <iostream>

namespace cxxfractal
{

/*----------------------------------------------------------------------------*/
inline static uint32_t SetBitmapColour(
	uint8_t r, 
	uint8_t g, 
	uint8_t b, 
	uint8_t a = 0)
{
	return (static_cast<uint32_t>(a) << 24)
		| (static_cast<uint32_t>(r) << 16)
		| (static_cast<uint32_t>(g) << 8)
		| (static_cast<uint32_t>(b));
}

/*----------------------------------------------------------------------------*/
uint32_t Mandelbrot::DefaultColourMap(size_t iterations, size_t iterationsMaximum)
{
	if (iterationsMaximum > 319)
	{
		iterationsMaximum = 319;
	}
	if (iterations == iterationsMaximum) 
	{ 
		return SetBitmapColour(0, 0, 0); 
	}
	else if (iterations < 16) 
	{ 
		return SetBitmapColour(
			static_cast<uint8_t>(8 * iterations), // 0 - 120
			static_cast<uint8_t>(8 * iterations), // 0 - 120
			static_cast<uint8_t>(128 + 4 * iterations)); // 128 - 188
	}
	else if (iterations < 64) 
	{ 
		return SetBitmapColour(
			static_cast<uint8_t>(128 + iterations - 16), // 128 - 192
			static_cast<uint8_t>(128 + iterations - 16), // 128 - 192
			static_cast<uint8_t>(192 + iterations - 16)); // 192 - 256 
	}
	else 
	{ 
		return SetBitmapColour(
			static_cast<uint8_t>(iterationsMaximum - iterations), // 0 - 254
			static_cast<uint8_t>(128 + (iterationsMaximum - iterations) / 2), // 128 - 255
			static_cast<uint8_t>(iterationsMaximum - iterations)); // 0 - 254
	}
}

/*----------------------------------------------------------------------------*/
HRESULT Mandelbrot::Draw(ID2D1Bitmap * bitmap)
{
	unsigned int amountThreads = std::thread::hardware_concurrency();

	if(nullptr == bitmap)
	{
		return S_FALSE;
	}

	D2D1_SIZE_U bmpSize = bitmap->GetPixelSize();
	size_t dataSize = static_cast<size_t>(bmpSize.width) * bmpSize.height;
	uint32_t * data = new uint32_t[dataSize];

#if 1
	Calculate(
		&data[0],
		bmpSize.width,
		bmpSize.height,
		offsetX_,
		offsetY_,
		zoom_,
		iterationsMaximum_);

	Colourise(data, dataSize, iterationsMaximum_, colourMap_);
#else
	size_t threadHeight = bmpSize.height / amountThreads;
	size_t dataStride   = bmpSize.width * threadHeight;
	double yStride      = static_cast<double>(threadHeight) / (zoom_ * xyRatio);

	std::vector<std::thread> threads;

	size_t idx = 0;
	double threadOffsetY = offsetY_;

	for(unsigned int threadnr = 0; threadnr < (amountThreads - 1); ++threadnr)
	{
		threads.emplace_back(
			Calculate, 
			&data[idx], 
			bmpSize.width,
			threadHeight,
			offsetX_,
			threadOffsetY,
			zoom_, 
			iterationsMaximum_);

		idx += dataStride;
		threadOffsetY += yStride;
	}

	for (auto& thread : threads)
	{
		thread.join();
	}
#endif
	D2D1_RECT_U rect = D2D1::RectU(0, 0, bmpSize.width, bmpSize.height);
	HRESULT hr = bitmap->CopyFromMemory(
		&rect,
		reinterpret_cast<void*>(&data[0]),
		4 * bmpSize.width);

	delete[] data;

	return hr;
}

/*----------------------------------------------------------------------------*/
void Mandelbrot::Calculate(
	uint32_t * iterations,
	size_t     width,
	size_t     height,
	double     offsetX,
	double     offsetY,
	double     zoom,
	size_t     iterationsMaximum)
{
	for (size_t ix = 0; ix < width; ++ix)
	{
		for (size_t iy = 0; iy < height; ++iy)
		{
			double cr = static_cast<double>(ix) / (zoom * static_cast<double>(width)) + offsetX;
			double ci = static_cast<double>(iy) / (xyRatio * zoom * static_cast<double>(height)) + offsetY;
			double zr = 0.0;
			double zi = 0.0;

			unsigned int i;
			double distance = (zr * zr + zi * zi);
			for (i = 0; (i < iterationsMaximum) && (distance < 4.0); ++i)
			{
				double zrt = zr * zr - zi * zi + cr;
				zi = 2.0 * zr * zi + ci;
				zr = zrt;
				distance = (zr * zr + zi * zi);
			}
			iterations[(iy * width) + ix] = i;
		}
	}
}

/*----------------------------------------------------------------------------*/
void Mandelbrot::Colourise(
	uint32_t  * data,
	size_t      amount,
	size_t      iterationsMaximum,
	ColourMap_t colourMap)
{
	for (size_t i = 0; i < amount; ++i)
	{
		data[i] = colourMap(data[i], iterationsMaximum);
	}
}

} /* namespace cxxfractal */ 
