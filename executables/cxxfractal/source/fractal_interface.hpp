/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief The fractal interface
 *
 * @author Frans Labuschagne
 * @date   2020-02-13
 *
 * @details
 * This file contains the fractal interface.
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H8CA5D4FE_2EE1_4B8F_9A6F_F97E98CD8F06
#define H8CA5D4FE_2EE1_4B8F_9A6F_F97E98CD8F06

#include <cstdint>
#include <d2d1.h>

namespace cxxfractal
{

class FractalInterface
{

public:
	/*! @brief Default constructor.
	 *
	 * @details
	 * This constructor shall initialise the FractalInterface to an invalid
	 * FractalInterface value.
	 */
	FractalInterface() = default;

	/*! @brief Default destructor.
	 *
	 * @details
	 * This destructor shall close the FractalInterface if it is valid.
	 */
	virtual
	~FractalInterface() noexcept = default;

	/*! @brief Copy construction.
	 *
	 * @details
	 * This constructor shall initialise the member variables with a copy of the
	 * source member variables.
	 *
	 * @param[in] source The source to copy the from.
	 */
	FractalInterface(FractalInterface const & source) = delete;

	/*! @brief Copy assignment.
	 *
	 * @details
	 * This assign operator shall assign the member variables from the other
	 * class to this class.
	 *
	 * @param[in] other The other class to assign to this class.
	 *
	 * @return This class with the other class member variables assigned to it.
	 */
	FractalInterface&
	operator=(FractalInterface const & other) = delete;

	/*! @brief Move construction.
	 *
	 * @details
	 * This constructor shall initialise the member variables by moving the
	 * source member variable to them.
	 *
	 * @param[in,out] source The source to move from.
	 */
	FractalInterface(FractalInterface && source) = delete;

	/*! @brief Move assignment.
	 *
	 * @details
	 * This move assign operator shall move the member variables from the other
	 * class to this class.
	 *
	 * @param[in,out] other The other class to move the member variables from.
	 *
	 * @return This class with the other class member variables moved to it.
	 */
	FractalInterface&
	operator=(FractalInterface && other) = delete;

	/*! @brief Draw the fractal.
     *
     * @details
     * Draw the fractal on the bitmap.
     *
     * @param[in] bitmap The bitmap to draw the fractal on.
     *
     * @return The result of drawing the fractal onto the bitmap.
     */
	virtual HRESULT Draw(ID2D1Bitmap * bitmap) = 0;

	virtual FractalInterface& SetOffsetX(double value) = 0;
	virtual FractalInterface& SetOffsetY(double value) = 0;
	virtual FractalInterface& SetZoom(double value) = 0;

	virtual double GetOffsetX() const = 0;
	virtual double GetOffsetY() const = 0;
	virtual double GetWidth() const = 0;
	virtual double GetHeight() const = 0;
	virtual double GetZoom() const = 0;

private:

};

} /* namespace cxxfractal */

#endif /* H8CA5D4FE_2EE1_4B8F_9A6F_F97E98CD8F06 */
