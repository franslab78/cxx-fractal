/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief The main windows application
 *
 * @author Frans Labuschagne
 * @date   2020-01-13
 *
 * @details
 * This file contains a class that create the main windows application.
 */
/*----------------------------------------------------------------------------*/

#include "fractal_application.hpp"
#include "mandelbrot.hpp"

#include <iostream>

namespace cxxfractal
{

/*----------------------------------------------------------------------------*/
FractalApplication::FractalApplication(int bmpWidth, int bmpHeight) :
		cxxwin::Application(
			"cxxfractal",
			"Fractals using C++",
			CW_USEDEFAULT, CW_USEDEFAULT, bmpWidth, bmpHeight),
			bmpWidth_(bmpWidth),
			bmpHeight_(bmpHeight),
			text_(L"Hello DirectWrite")
{
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&frequency_));

	HRESULT hrslt = directWriteFactory_->CreateTextFormat(
		L"Gabriola",                // Font family name.
		NULL,                       // Font collection (NULL sets it to use the system font collection).
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		72.0f,
		L"en-us",
		&writeTextFormat_);
	if(FAILED(hrslt))
	{
		std::string msg("Could not create direct write text format ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}
	hrslt = writeTextFormat_->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	if(FAILED(hrslt))
	{
		std::string msg("Could not set direct write text format alignment to centre ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}
	hrslt = writeTextFormat_->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	if(FAILED(hrslt))
	{
		std::string msg("Could not set direct write paragraph format alignment to centre ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}

	OnDestroy.connect<FractalApplication, &FractalApplication::HandleDestroy>(this);
	OnDisplayChange.connect<FractalApplication, &FractalApplication::HandleDisplayChange>(this);
	OnSize.connect<FractalApplication, &FractalApplication::HandleResize>(this);
	OnPaint.connect<FractalApplication, &FractalApplication::HandlePaint>(this);
	OnKey.connect<FractalApplication, &FractalApplication::HandleKey>(this);
}

/*----------------------------------------------------------------------------*/
FractalApplication::~FractalApplication()
{
	if(nullptr != bitmap_)
	{
		bitmap_->Release();
		bitmap_ = nullptr;
	}
}

/*----------------------------------------------------------------------------*/
bool FractalApplication::HandleDestroy()
{
	PostQuitMessage(0);
	return true;
}

/*----------------------------------------------------------------------------*/
bool FractalApplication::HandleDisplayChange(int depth, int width, int height)
{
	(void)depth; /* Unused parameter */
	(void)width; /* Unused parameter */
	(void)height; /* Unused parameter */
	InvalidateRect(windowHandle_, NULL, FALSE);
	return true;
}

/*----------------------------------------------------------------------------*/
bool FractalApplication::HandleResize(cxxwin::ResizeTypes how, int width, int height)
{
	if(how != cxxwin::ResizeTypes::Minimised)
	{
		if (renderTarget_)
		{
			renderTarget_->Resize(D2D1::SizeU(width, height));
			CreateBitmap(width, height);
			return true;
		}
	}
	return false;
}

/*----------------------------------------------------------------------------*/
bool FractalApplication::HandlePaint()
{
	HRESULT hrslt = S_OK;

	hrslt = CreateDeviceResources();
	if (SUCCEEDED(hrslt))
	{
		renderTarget_->BeginDraw();

		renderTarget_->SetTransform(D2D1::Matrix3x2F::Identity());

		renderTarget_->Clear(D2D1::ColorF(D2D1::ColorF::White));

		D2D1_SIZE_F rtSize = renderTarget_->GetSize();
		D2D1_SIZE_F bmpSize = bitmap_->GetSize();

		D2D1_POINT_2F bmpUpperLeftCorner = D2D1::Point2F((rtSize.width - bmpSize.width)/2, (rtSize.height - bmpSize.height)/2);

		renderTarget_->DrawBitmap(bitmap_,
			D2D1::RectF(
				bmpUpperLeftCorner.x,
				bmpUpperLeftCorner.y,
				bmpUpperLeftCorner.x + bmpSize.width,
				bmpUpperLeftCorner.y + bmpSize.height));

		D2D1_RECT_F layoutRect = D2D1::RectF(
			static_cast<FLOAT>(0),
			static_cast<FLOAT>(0),
			static_cast<FLOAT>(rtSize.width),
			static_cast<FLOAT>(rtSize.height)
		);

		renderTarget_->DrawText(
			text_.c_str(),        // The string to render.
			text_.size(),    // The string's length.
			writeTextFormat_,    // The text format.
			layoutRect,       // The region of the window where the text will be rendered.
			txtBrush_     // The brush used to draw the text.
		);


		hrslt = renderTarget_->EndDraw();
	}
	if (hrslt == D2DERR_RECREATE_TARGET)
	{
		hrslt = S_OK;
		DiscardDeviceResources();
	}
	ValidateRect(windowHandle_, NULL);

	return true;
}

/*----------------------------------------------------------------------------*/
bool FractalApplication::HandleKey(cxxwin::KeyDirections        direction,
	unsigned                    virtualCode,
	unsigned                    scanCode,
	unsigned                    repeatCount,
	cxxwin::KeyStateFlags::type flags)
{
	(void)(scanCode);
	(void)(repeatCount);
	(void)(flags);

	if(direction == cxxwin::KeyDirections::Character)
	{
		bool update = false;
		double zoom = fractal_->GetZoom();

		switch (virtualCode)
		{
			case 'w':
			{
				double height = fractal_->GetHeight();
				double offset = fractal_->GetOffsetY();
				offset -= height / 10;
				fractal_->SetOffsetY(offset);
				update = true;
				break;
			}
			case 's':
			{
				double height = fractal_->GetHeight();
				double offset = fractal_->GetOffsetY();
				offset += height / 10;
				fractal_->SetOffsetY(offset);
				update = true;
				break;
			}
			case 'a':
			{
				double width = fractal_->GetWidth();
				double offset = fractal_->GetOffsetX();
				offset -= width / 10;
				fractal_->SetOffsetX(offset);
				update = true;
				break;
			}
			case 'd':
			{
				double width = fractal_->GetWidth();
				double offset = fractal_->GetOffsetX();
				offset += width / 10;
				fractal_->SetOffsetX(offset);
				update = true;
				break;
			}
			case '+':
			{
				fractal_->SetZoom(zoom + 0.1);
				update = true;
				break;
			}
			case '-':
			{
				fractal_->SetZoom(zoom - 0.1);
				update = true;
				break;
			}
		}
		if (update == true)
		{
			UpdateBitmap();
			InvalidateRect(windowHandle_, NULL, FALSE);
			//UpdateWindow(windowHandle_);
			return true;
		}
	}
	return false;
}

/*----------------------------------------------------------------------------*/
HRESULT FractalApplication::CreateDeviceResources()
{
	HRESULT hrslt = S_OK;

	if (!renderTarget_)
	{
		hrslt = Application::CreateDeviceResources();

		if (SUCCEEDED(hrslt))
		{
			hrslt = CreateBitmap(bmpWidth_, bmpHeight_);
		}
		if (SUCCEEDED(hrslt))
		 {
			  hrslt = renderTarget_->CreateSolidColorBrush(
					D2D1::ColorF(D2D1::ColorF::Black),
					&txtBrush_);
		 }
	}


	return hrslt;
}

/*----------------------------------------------------------------------------*/
HRESULT FractalApplication::CreateBitmap(int width, int height)
{
	HRESULT hrslt = S_OK;
	if (nullptr != renderTarget_)
	{
		if((width != bmpWidth_) || (height != bmpHeight_))
		{
			if(nullptr != bitmap_)
			{
				bitmap_->Release();
				bitmap_ = nullptr;
			}
		}

		D2D1_PIXEL_FORMAT pixelFormat = renderTarget_->GetPixelFormat();
		FLOAT dpiX, dpiY;
		renderTarget_->GetDpi(&dpiX, &dpiY);

		// Create a bitmap.
		hrslt = renderTarget_->CreateBitmap(
			D2D1::SizeU(width, height),
			D2D1::BitmapProperties(pixelFormat, dpiX, dpiY),
			&bitmap_
		);
		if (SUCCEEDED(hrslt))
		{
			bmpWidth_ = width;
			bmpHeight_ = height;
			hrslt = UpdateBitmap();
		}
	}
	return hrslt;
}

/*----------------------------------------------------------------------------*/
HRESULT FractalApplication::UpdateBitmap()
{
	uint64_t start, stop;
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&start));
	HRESULT hrslt = fractal_->Draw(bitmap_);
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&stop));
	double elapsed = static_cast<double>(stop - start) / static_cast<double>(frequency_);
	std::string estr = std::to_string(elapsed);
	std::cout << elapsed << std::endl;
	OutputDebugStringA(estr.c_str());
	return hrslt;
}


/*----------------------------------------------------------------------------*/
void FractalApplication::DiscardDeviceResources()
{
	if(nullptr != txtBrush_)
	{
		txtBrush_->Release();
		txtBrush_ = nullptr;
	}
	if(nullptr != bitmap_)
	{
		bitmap_->Release();
		bitmap_ = nullptr;
	}
	Application::DiscardDeviceResources();
}

} /* namespace cxxfractal */
