/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @files
 *
 * @brief The Mandelbrot
 *
 * @author Frans Labuschagne
 * @date   2020-02-13
 *
 * @details
 * This file contains a Mandelbrot.
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef hf72a5f78_c696_4b9d_898f_e9f249adbc88
#define hf72a5f78_c696_4b9d_898f_e9f249adbc88

#include "fractal_interface.hpp"
#include <functional>
#include <thread>

namespace cxxfractal
{

class Mandelbrot : public FractalInterface
{
public:
	using ColourMap_t = std::function<uint32_t (size_t, size_t)>;

	static uint32_t DefaultColourMap(size_t iterations, size_t iterationsMaximum);

	/*! @brief Default constructor.
	 *
	 * @details
	 * This constructor shall initialise the Mandelbrot to an invalid Mandelbrot value.
	 * The class derived from the Mandelbrot class needs to create the Mandelbrot.
	 */
	Mandelbrot() = default;

	/*! @brief Default destructor.
	 *
	 * @details
	 * This destructor shall close the Mandelbrot if it is valid.
	 */
	virtual
	~Mandelbrot() noexcept = default;

	/*! @brief Copy construction.
	 *
	 * @details
	 * This constructor shall initialise the member variables with a copy of the
	 * source member variables.
	 *
	 * @param[in] source The source to copy the from.
	 */
	Mandelbrot(Mandelbrot const & source) = delete;

	/*! @brief Copy assignment.
	 *
	 * @details
	 * This assign operator shall assign the member variables from the other
	 * class to this class.
	 *
	 * @param[in] other The other class to assign to this class.
	 *
	 * @return This class with the other class member variables assigned to it.
	 */
	Mandelbrot&
	operator=(Mandelbrot const & other) = delete;

	/*! @brief Move construction.
	 *
	 * @details
	 * This constructor shall initialise the member variables by moving the
	 * source member variable to them.
	 *
	 * @param[in,out] source The source to move from.
	 */
	Mandelbrot(Mandelbrot && source) = default;

	/*! @brief Move assignment.
	 *
	 * @details
	 * This move assign operator shall move the member variables from the other
	 * class to this class.
	 *
	 * @param[in,out] other The other class to move the member variables from.
	 *
	 * @return This class with the other class member variables moved to it.
	 */
	Mandelbrot&
	operator=(Mandelbrot && other) = default;

	/*! @brief Default constructor.
	 *
	 * @details
	 * This constructor shall initialise the Mandelbrot to an invalid Mandelbrot value.
	 * The class derived from the Mandelbrot class needs to create the Mandelbrot.
	 */
	Mandelbrot(
		double offsetX,
		double offsetY,
		double zoom,
		size_t iterationsMaximum,
		ColourMap_t colourMap) :
			offsetX_(offsetX),
			offsetY_(offsetY),
			zoom_(zoom),
			iterationsMaximum_(iterationsMaximum),
			colourMap_(colourMap)
	{
	}

	/*! @brief Draw the fractal.
	 *
	 * @details
	 * Draw the fractal on the bitmap.
	 *
	 * @param[in] bitmap The bitmap to draw the fractal on.
	 *
	 * @return The result of drawing the fractal onto the bitmap.
	 */
	HRESULT Draw(ID2D1Bitmap * bitmap) override;

	Mandelbrot& SetOffsetX(double value) { if (value != offsetX_) { offsetX_ = value; } return *this; }
	Mandelbrot& SetOffsetY(double value) { if (value != offsetY_) { offsetY_ = value; } return *this; }
	Mandelbrot& SetZoom(double value) { if (value != zoom_) { zoom_ = value; } return *this; }

	double GetOffsetX() const { return offsetX_; }
	double GetOffsetY() const { return offsetY_; }
	double GetWidth() const { return 1.0 / zoom_; };
	double GetHeight() const { return 1.0 / (xyRatio * zoom_);};
	double GetZoom() const { return zoom_; }

private:
	/*! The ratio beween the x-y */
	static constexpr double xyRatio = (2.0) / (3.5);

	/*! @brief Calculate the Mandelbrot set.
	 *
	 * @details
	 * This operation calculates the Mandelbrot set.
	 *
	 * @param[out] iterations The amount of iterations
	 * @param[in]  width      The width of the image
	 * @param[in]  height     The height of the image
	 * @param[in]  offsetX    The x (real) offset in the complex plain.
	 * @param[in]  offsetY    The y (imaganery) offset in the complex plain.
	 * @param[in]  zoom       The zoom factor.
	 */
	static void Calculate(
		uint32_t *  iterations,
		size_t      width,
		size_t      height,
		double      offsetX,
		double      offsetY,
		double      zoom,
		size_t      iterationsMaximum);

	static void Colourise(
		uint32_t* data,
		size_t    amount,
		size_t     iterationsMaximum,
		ColourMap_t colourMap);

	/*! The offset in x */
	double offsetX_ = -2.5;
	/*! The offset in y */
	double offsetY_ = -1.0;
	/*! The zoom factor. */
	double zoom_ = 1.0;
	/*! The maximum amount of iterations. */
	size_t iterationsMaximum_ = 200;
	/*! The colour max to use. */
	ColourMap_t colourMap_ = DefaultColourMap;
};

} /* namespace cxxfractal */

#endif /* hf72a5f78_c696_4b9d_898f_e9f249adbc88 */
