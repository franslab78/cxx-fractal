/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The main windows application
 *
 * This file contains a class that create the main windows application.
 *
 * @author Frans Labuschagne
 * @date   2020-01-13
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/application.hpp"

#include <stdexcept>
#include <cmath>

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
LRESULT __stdcall Application::WndProc(
	HWND const   window,
	UINT const   message,
	WPARAM const wparam,
	LPARAM const lparam)
{
#ifdef USE_SPDLOG
	//spdlog::trace(Messages::MessageToString(message));
#endif

	if (WM_NCCREATE == message)
	{
		auto cs = reinterpret_cast<CREATESTRUCT*>(lparam);
		Application* that = static_cast<Application*>(cs->lpCreateParams);
		if (nullptr == that)
		{
			throw std::runtime_error("Invalid parameter passed to windows create");
		}
		if (nullptr != that->windowHandle_)
		{
			throw std::runtime_error("Valid windows handle?");
		}
		SetWindowLongPtr(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(that));
		that->windowHandle_ = window;
		if(!that->OnNcCreate.is_empty())
		{
			std::vector<bool> results;
			auto accumulator = [&](bool srv) { results.push_back(srv);	};

			that->OnCreate.fire_accumulate(accumulator, cs);
			if(std::any_of(results.begin(), results.end(), [](bool result){ return result == false; }))
			{
				return FALSE;
			}
			return TRUE;
		}
	}
#if 0
	if (WM_NCDESTROY == message)
	{
		Application* that = static_cast<Application*>(GetWindowLongPtrW(window, GWLP_USERDATA));
		if(!that->OnNcCreate.is_empty())
		{
			that->OnNcDestroy.fire();
			return 0;
		}
	}
	else
#endif
	{
		Application* that = reinterpret_cast<Application*>(GetWindowLongPtrW(window, GWLP_USERDATA));
		if(nullptr != that)
		{
			return that->MessageHandler(message, wparam, lparam);
		}
	}

	return DefWindowProc(window, message, wparam, lparam);
}

/*----------------------------------------------------------------------------*/
std::wstring Application::toWideString(std::string_view const string)
{
	int length = static_cast<int>(string.size());
	int size_needed = MultiByteToWideChar(
		/* .CodePage       = */ CP_UTF8,
		/* .dwFlags        = */ 0,
		/* .lpMultiByteStr = */ string.data(),
		/* .cbMultiByte    = */ length,
		/* .lpWideCharStr  = */ nullptr,
		/* .cchWideChar    = */ 0);

	std::wstring wstrTo(static_cast<std::wstring::size_type>(size_needed), 0);

	MultiByteToWideChar(
		/* .CodePage       = */ CP_UTF8,
		/* .dwFlags        = */ 0,
		/* .lpMultiByteStr = */ string.data(),
		/* .cbMultiByte    = */ length,
		/* .lpWideCharStr  = */ &wstrTo[0],
		/* .cchWideChar    = */ size_needed);

	return wstrTo;
}

#if 0
/*----------------------------------------------------------------------------*/
static std::string toString(LPCWSTR string)
{
	int length = static_cast<int>(wcslen(string));
	int size_needed = WideCharToMultiByte(
		/* .CodePage          = */ CP_UTF8,
		/* .dwFlags           = */ 0,
		/* .lpWideCharStr     = */ string,
		/* .cchWideChar       = */ length,
		/* .lpMultiByteStr    = */ nullptr,
		/* .cbMultiByte       = */ 0,
		/* .lpDefaultChar     = */ nullptr,
		/* .lpUsedDefaultChar = */ nullptr);

	std::string strTo(static_cast<std::string::size_type>(size_needed), 0);

	WideCharToMultiByte(
		/* .CodePage          = */ CP_UTF8,
		/* .dwFlags           = */ 0,
		/* .lpWideCharStr     = */ string,
		/* .cchWideChar       = */ length,
		/* .lpMultiByteStr    = */ &strTo[0],
		/* .cbMultiByte       = */ size_needed,
		/* .lpDefaultChar     = */ nullptr,
		/* .lpUsedDefaultChar = */ nullptr);

	return strTo;
}
#else
/*----------------------------------------------------------------------------*/
std::string Application::toString(std::wstring_view const string)
{
	int length = static_cast<int>(string.size());
	int size_needed = WideCharToMultiByte(
		/* .CodePage          = */ CP_UTF8,
		/* .dwFlags           = */ 0,
		/* .lpWideCharStr     = */ string.data(),
		/* .cchWideChar       = */ length,
		/* .lpMultiByteStr    = */ nullptr,
		/* .cbMultiByte       = */ 0,
		/* .lpDefaultChar     = */ nullptr,
		/* .lpUsedDefaultChar = */ nullptr);

	std::string strTo(static_cast<std::string::size_type>(size_needed), 0);

	WideCharToMultiByte(
		/* .CodePage          = */ CP_UTF8,
		/* .dwFlags           = */ 0,
		/* .lpWideCharStr     = */ string.data(),
		/* .cchWideChar       = */ length,
		/* .lpMultiByteStr    = */ &strTo[0],
		/* .cbMultiByte       = */ size_needed,
		/* .lpDefaultChar     = */ nullptr,
		/* .lpUsedDefaultChar = */ nullptr);

	return strTo;
}
#endif

/*----------------------------------------------------------------------------*/
std::string Application::DescriptionLastError(DWORD code)
{
	LPVOID pBuffer;

	FormatMessageW(
		/* .dwFlags      = */ FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_IGNORE_INSERTS
		| FORMAT_MESSAGE_ALLOCATE_BUFFER,
		/* .lpSource     = */ nullptr,
		/* .dwMessageId  = */ code,
		/* .dwLanguageId = */ MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		/* .lpBuffer     = */ reinterpret_cast<LPWSTR>(&pBuffer),
		/* .nSize        = */ 0,
		/* .Arguments    = */ nullptr);
	std::string ret = toString(static_cast<LPCWSTR>(pBuffer));
	LocalFree(pBuffer);
	return ret;
}

/*----------------------------------------------------------------------------*/
std::string Application::DescriptionHResult(HRESULT code)
{
	return DescriptionLastError(static_cast<DWORD>(HRESULT_CODE(code)));
}

/*----------------------------------------------------------------------------*/
Application::~Application() noexcept
{
	if(nullptr != renderTarget_)
	{
		renderTarget_->Release();
		renderTarget_ = nullptr;
	}
	if(nullptr != direct2dFactory_)
	{
		direct2dFactory_->Release();
		direct2dFactory_ = nullptr;
	}
	if(nullptr != directWriteFactory_)
	{
		directWriteFactory_->Release();
		directWriteFactory_ = nullptr;
	}
	if(0 != atom_)
	{
		UnregisterClassW(reinterpret_cast<LPCWSTR>(atom_), nullptr);
		atom_ = 0;
	}
}

/*----------------------------------------------------------------------------*/
Application::Application(
	const std::string_view      name,
	const std::string_view      caption,
	int                         x,
	int                         y,
	int                         width,
	int                         height,
	WindowStyleFlags::type      windowStyle,
	WindowClassStyleFlags::type windowClassStyle)
{
	HRESULT hrslt = CoInitialize(nullptr);
	if(FAILED(hrslt))
	{
		std::string msg("Could not initialise COM ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}

	hrslt = D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		&direct2dFactory_);
	if(FAILED(hrslt))
	{
		std::string msg("Could not create Direct2D factory ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}

	hrslt = DWriteCreateFactory(
        DWRITE_FACTORY_TYPE_SHARED,
        __uuidof(IDWriteFactory),
        reinterpret_cast<IUnknown**>(&directWriteFactory_));
	if(FAILED(hrslt))
	{
		std::string msg("Could not create DirectWrite factory ");
		msg += DescriptionHResult(hrslt);
		throw std::runtime_error(msg);
	}

	std::wstring wname = toWideString(name);
	WNDCLASSEXW info = {
		/* .cbSize        = */ static_cast<UINT>(sizeof(WNDCLASSEX)),
		/* .style         = */ static_cast<UINT>(windowClassStyle),
		/* .lpfnWndProc   = */ static_cast<WNDPROC>(WndProc),
		/* .cbClsExtra    = */ 0,
		/* .cbWndExtra    = */ static_cast<int>(sizeof(uintptr_t)),
		/* .hInstance     = */ nullptr,
		/* .hIcon         = */ /*static_cast<HICON>*/(nullptr),
		/* .hCursor       = */ /*static_cast<HCURSOR>*/(nullptr),
		/* .hbrBackground = */ /*static_cast<HBRUSH>*/(nullptr),
		/* .lpszMenuName  = */ /*static_cast<TCHAR const *>*/(nullptr),
		/* .lpszClassName = */ wname.c_str(),
		/* .hIconSm       = */ /*static_cast<HICON>*/(nullptr) };

	atom_ = RegisterClassExW(&info);
	if(0 == atom_)
	{
		std::string msg("Could not register WindowClass:\"");
		msg += name;
		msg += "\"";
		DWORD code = GetLastError();
		msg += DescriptionLastError(code);
		direct2dFactory_->Release();
		direct2dFactory_ = nullptr;
		directWriteFactory_->Release();
		directWriteFactory_ = nullptr;
		throw std::runtime_error(msg);
	}

	DWORD dwExStyle = static_cast<DWORD>(windowStyle >> 32);
	DWORD dwStyle   = static_cast<DWORD>(windowStyle & 0xFFFFFFFFu);

	std::wstring wcaption = toWideString(caption);

	windowHandle_ = CreateWindowExW(
		/* .dwExStyle    = */ dwExStyle,
		/* .lpClassName  = */ reinterpret_cast<LPCWSTR>(atom_),
		/* .lpWindowName = */ wname.c_str(),
		/* .dwStyle      = */ dwStyle,
		/* .X            = */ x,
		/* .Y            = */ y,
		/* .nWidth       = */ width,
		/* .nHeight      = */ height,
		/* .hWndParent   = */ nullptr,
		/* .hMenu        = */ nullptr,
		/* .hInstance    = */ nullptr,
		/* .lpParam      = */ this);
	if(0 == windowHandle_)
	{
		std::string msg("Could not create Window ");
		DWORD code = GetLastError();
		msg += DescriptionLastError(code);
		direct2dFactory_->Release();
		direct2dFactory_ = nullptr;
		directWriteFactory_->Release();
		directWriteFactory_ = nullptr;
		UnregisterClassW(reinterpret_cast<LPCWSTR>(atom_), nullptr);
		atom_ = 0;
		throw std::runtime_error(msg);
	}
}

/*----------------------------------------------------------------------------*/
int Application::Execute(ShowCommands show)
{
	ShowWindow(windowHandle_, static_cast<int>(show));
	UpdateWindow(windowHandle_);

	BOOL rslt;
	MSG msg;

	while ((rslt = GetMessage(&msg, nullptr, 0, 0)) != 0)
	{
		if(rslt == -1)
		{
			std::string emsg("Could not get message ");
			DWORD code = GetLastError();
			emsg += DescriptionLastError(code);
			direct2dFactory_->Release();
			direct2dFactory_ = nullptr;
			directWriteFactory_->Release();
		   directWriteFactory_ = nullptr;
			UnregisterClassW(reinterpret_cast<LPCWSTR>(atom_), nullptr);
			atom_ = 0;
			throw std::runtime_error(emsg);
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return static_cast<int>(msg.wParam);
}

/*----------------------------------------------------------------------------*/
HRESULT Application::CreateDeviceResources()
{
	HRESULT hr = S_OK;

	if (!renderTarget_)
	{
		RECT rc;
		GetClientRect(windowHandle_, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(
			rc.right - rc.left,
			rc.bottom - rc.top
		);

		// Create a Direct2D render target.
		hr = direct2dFactory_->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(windowHandle_, size),
			&renderTarget_
		);
	}
	return hr;
}

/*----------------------------------------------------------------------------*/
void Application::DiscardDeviceResources()
{
	if(nullptr != renderTarget_)
	{
		renderTarget_->Release();
		renderTarget_ = nullptr;
	}
}

/*----------------------------------------------------------------------------*/
[[nodiscard]] LRESULT
Application::MessageHandler(
	UINT const message,
	WPARAM const wparam,
	LPARAM const lparam)
{
	if(message < WM_USER)
	{
		/* Messages reserved for use by the system. */
		return SystemMessageHandler(message, wparam, lparam);
	}
	else if(message < WM_APP)
	{
		/* Integer messages for use by private window classes. */
		return UserMessageHandler(message, wparam, lparam);
	}
	else if(message < 0xCFFF)
	{
		/* Messages available for use by applications. */
		return ApplicationMessageHandler(message, wparam, lparam);
	}

	return DefWindowProc(windowHandle_, message, wparam, lparam);
}

/*----------------------------------------------------------------------------*/
[[nodiscard]] LRESULT
Application::UserMessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam)
{
	return DefWindowProc(windowHandle_, message, wparam, lparam);
}

/*----------------------------------------------------------------------------*/
[[nodiscard]] LRESULT
Application::ApplicationMessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam)
{
	return DefWindowProc(windowHandle_, message, wparam, lparam);
}

/*----------------------------------------------------------------------------*/
[[nodiscard]] LRESULT
Application::SystemMessageHandler(
	UINT const message,
	WPARAM const wparam,
	LPARAM const lparam)
{
	KeyDirections     keyDirection         = KeyDirections::Last;
	ButtonDirections  mouseButtonDirection = ButtonDirections::Last;
	ScrollDirections  scrollDirection      = ScrollDirections::Last;
	MouseButtons      mouseButton          = MouseButtons::Last;

	std::vector<bool> results;

	switch(message)
	{
		case WM_NULL:
			if(!OnNull.is_empty())
			{
				OnNull.fire(wparam, lparam);
				return 0;
			}
			break;

		case WM_CREATE:
			if(!OnCreate.is_empty())
			{
				CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>(lparam);

				OnCreate.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					cs);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == false; }))
				{
					return -1;
				}
				return 0;
			}
			break;

		case WM_DESTROY:
			if(!OnDestroy.is_empty())
			{
				OnDestroy.fire_accumulate([&](bool srv) { results.push_back(srv); });

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_MOVE:
			if(!OnMove.is_empty())
			{
				int x = static_cast<int>(LOWORD(lparam));
				int y = static_cast<int>(HIWORD(lparam));

				OnMove.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					x, y);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_SIZE:
			if(!OnSize.is_empty())
			{
				ResizeTypes how    = static_cast<ResizeTypes::value_t>(wparam);
				int         width  = static_cast<int>(LOWORD(lparam));
				int         height = static_cast<int>(HIWORD(lparam));

				OnSize.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					how, width, height);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_ACTIVATE:
			if(!OnActivate.is_empty())
			{
				ActivateTypes how         = static_cast<ActivateTypes::value_t>(LOWORD(wparam));
				bool          isMinimized = static_cast<bool>(HIWORD(wparam));
				HWND          hwnd        = reinterpret_cast<HWND>(lparam);

				OnActivate.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					how, isMinimized, hwnd);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_SETFOCUS:
			if(!OnFocus.is_empty())
			{
				HWND hwnd = reinterpret_cast<HWND>(wparam);

				OnFocus.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					true, hwnd);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_KILLFOCUS:
			if(!OnFocus.is_empty())
			{
				HWND hwnd = reinterpret_cast<HWND>(wparam);

				OnFocus.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					false, hwnd);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_ENABLE:
			if(!OnEnable.is_empty())
			{
				bool isEnable = static_cast<bool>(wparam);

				OnEnable.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					isEnable);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;
#if 0
		case WM_SETREDRAW:
			break;

		case WM_SETTEXT:
			break;

		case WM_GETTEXT:
			break;

		case WM_GETTEXTLENGTH:
			break;
#endif

		case WM_PAINT:
			if(!OnPaint.is_empty())
			{
				OnPaint.fire_accumulate([&](bool srv) { results.push_back(srv); });

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_CLOSE:
			if(!OnClose.is_empty())
			{
				OnClose.fire_accumulate([&](bool srv) { results.push_back(srv); });

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_ERASEBKGND:
			if(!OnEraseBackground.is_empty())
			{
				HDC hdc = reinterpret_cast<HDC>(wparam);

				OnEraseBackground.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					hdc);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_SYSCOLORCHANGE:
			break;

#endif
		case WM_SHOWWINDOW:
			if(!OnShowWindow.is_empty())
			{
				bool               isShown = static_cast<bool>(wparam);
				ShowWindowStatuses status  = static_cast<ShowWindowStatuses::value_t>(lparam);

				OnShowWindow.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					isShown, status);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_SETTINGCHANGE:
			break;

		case WM_DEVMODECHANGE:
			break;

		case WM_ACTIVATEAPP:
			break;

		case WM_FONTCHANGE:
			break;

		case WM_TIMECHANGE:
			break;

		case WM_CANCELMODE:
			break;

#endif
		case WM_SETCURSOR:
			if(!OnSetCursor.is_empty())
			{
				HWND     hwnd = reinterpret_cast<HWND>(wparam);
				unsigned msg  = static_cast<unsigned>(HIWORD(lparam));
				int      x    = static_cast<int>(GET_X_LPARAM(lparam));
				int      y    = static_cast<int>(GET_Y_LPARAM(lparam));

				OnSetCursor.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					hwnd, msg, x, y);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_MOUSEACTIVATE:
			break;

		case WM_CHILDACTIVATE:
			break;

		case WM_QUEUESYNC:
			break;

		case WM_GETMINMAXINFO:
			break;

		case WM_PAINTICON:
			break;

		case WM_ICONERASEBKGND:
			break;

		case WM_NEXTDLGCTL:
			break;

		case WM_SPOOLERSTATUS:
			break;

		case WM_DRAWITEM:
			break;

		case WM_MEASUREITEM:
			break;

		case WM_DELETEITEM:
			break;

		case WM_VKEYTOITEM:
			break;

		case WM_CHARTOITEM:
			break;

		case WM_SETFONT:
			break;

		case WM_GETFONT:
			break;

		case WM_SETHOTKEY:
			break;

		case WM_GETHOTKEY:
			break;

		case WM_QUERYDRAGICON:
			break;

		case WM_COMPAREITEM:
			break;

		case WM_COMPACTING:
			break;

		case WM_COMMNOTIFY:
			break;

		case WM_WINDOWPOSCHANGING:
			break;

		case WM_WINDOWPOSCHANGED:
			break;

		case WM_POWER:
			break;

		case WM_COPYDATA:
			break;

		case WM_CANCELJOURNAL:
			break;

		case WM_NOTIFY:
			break;

		case WM_INPUTLANGCHANGEREQUEST:
			break;

		case WM_INPUTLANGCHANGE:
			break;

		case WM_TCARD:
			break;

		case WM_HELP:
			break;

		case WM_USERCHANGED:
			break;

		case WM_NOTIFYFORMAT:
			break;

		case WM_CONTEXTMENU:
			break;

		case WM_STYLECHANGING:
			break;

		case WM_STYLECHANGED:
			break;

#endif
		case WM_DISPLAYCHANGE:
			if(!OnSize.is_empty())
			{
				int depth  = static_cast<int>(wparam);
				int width  = static_cast<int>(LOWORD(lparam));
				int height = static_cast<int>(HIWORD(lparam));

				OnDisplayChange.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					depth, width, height);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_GETICON:
			break;

		case WM_SETICON:
			break;

#endif
		case WM_NCDESTROY:
			if(!OnNcDestroy.is_empty())
			{
				OnNcDestroy.fire_accumulate([&](bool srv) { results.push_back(srv); });

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_NCCALCSIZE:
			break;

		case WM_NCHITTEST:
			break;

		case WM_NCHITTEST:
			break;

#endif
		case WM_NCPAINT:
			if(!OnNcPaint.is_empty())
			{
				HRGN updateRegion = reinterpret_cast<HRGN>(wparam);
				HDC hdc = GetDCEx(windowHandle_, updateRegion, DCX_WINDOW|DCX_INTERSECTRGN);

				OnNcPaint.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					hdc, updateRegion);

				ReleaseDC(windowHandle_, hdc);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_NCACTIVATE:
			break;

		case WM_GETDLGCODE:
			break;

		case WM_NCMOUSEMOVE:
			break;

		case WM_NCLBUTTONDOWN:
			break;

		case WM_NCLBUTTONUP:
			break;

		case WM_NCLBUTTONDBLCLK:
			break;

		case WM_NCRBUTTONDOWN:
			break;

		case WM_NCRBUTTONUP:
			break;

		case WM_NCRBUTTONDBLCLK:
			break;

		case WM_NCMBUTTONDOWN:
			break;

		case WM_NCMBUTTONUP:
			break;

		case WM_NCMBUTTONDBLCLK:
			break;

		case WM_NCXBUTTONDOWN:
			break;

		case WM_NCXBUTTONUP:
			break;

		case WM_INPUT_DEVICE_CHANGE:
			break;

		case WM_INPUT:
			break;

#endif
		case WM_KEYDOWN:
			keyDirection = KeyDirections::Down;
			goto key_event;
		case WM_KEYUP:
			keyDirection = KeyDirections::Up;
			goto key_event;
		case WM_CHAR:
			keyDirection = KeyDirections::Character;
			goto key_event;
		case WM_SYSKEYDOWN:
			keyDirection = KeyDirections::SystemDown;
			goto key_event;
		case WM_SYSKEYUP:
			keyDirection = KeyDirections::SystemUp;
			goto key_event;
		case WM_SYSCHAR:
			keyDirection = KeyDirections::SystemCharacter;
key_event:
			if(!OnKey.is_empty())
			{
				unsigned virtualCode      = static_cast<unsigned>(wparam);
				unsigned repeatCount      = static_cast<unsigned>(lparam) & 0x0000FFFF;
				unsigned scanCode         = (static_cast<unsigned>(lparam) & 0x00FF0000) >> 16;
				KeyStateFlags::type flags = static_cast<KeyStateFlags::type>(lparam) & KeyStateFlags::MaskAll;

				OnKey.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					keyDirection, virtualCode, scanCode, repeatCount, flags);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;
#if 0
		case WM_DEADCHAR:
			break;

		case WM_SYSDEADCHAR:
			break;

		case WM_UNICHAR:
			break;

		case WM_IME_STARTCOMPOSITION:
			break;

		case WM_IME_ENDCOMPOSITION:
			break;

		case WM_IME_COMPOSITION:
			break;

		case WM_INITDIALOG:
			break;

		case WM_COMMAND:
			break;

		case WM_SYSCOMMAND:
			break;

		case WM_TIMER:
			break;

		case WM_HSCROLL:
			break;

		case WM_VSCROLL:
			break;

		case WM_INITMENU:
			break;

		case WM_INITMENUPOPUP:
			break;

		case WM_MENUSELECT:
			break;

#if _WIN32_WINNT >= _WIN32_WINNT_WIN7
		case WM_GESTURE:
			break;

		case WM_GESTURENOTIFY:
			break;

#endif
		case WM_MENUCHAR:
			break;

		case WM_ENTERIDLE:
			break;

		case WM_MENURBUTTONUP:
			break;

		case WM_MENUDRAG:
			break;

		case WM_MENUGETOBJECT:
			break;

		case WM_UNINITMENUPOPUP:
			break;

		case WM_MENUCOMMAND:
			break;

		case WM_CHANGEUISTATE:
			break;

		case WM_UPDATEUISTATE:
			break;

		case WM_QUERYUISTATE:
			break;

		case WM_CTLCOLORMSGBOX:
			break;

		case WM_CTLCOLOREDIT:
			break;

		case WM_CTLCOLORLISTBOX:
			break;

		case WM_CTLCOLORBTN:
			break;

		case WM_CTLCOLORDLG:
			break;

		case WM_CTLCOLORSCROLLBAR:
			break;

		case WM_CTLCOLORSTATIC:
			break;

		case MN_GETHMENU:
			break;

#endif
		case WM_MOUSEMOVE:
			if(!OnMouseMove.is_empty())
			{
				int                        x           = static_cast<int>(GET_X_LPARAM(lparam));
				int                        y           = static_cast<int>(GET_Y_LPARAM(lparam));
				MouseVirtualKeyFlags::type virtualKeys = static_cast<MouseVirtualKeyFlags::type>(wparam);

				OnMouseMove.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					x, y, virtualKeys);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_LBUTTONDOWN: /* Intentional fall through */
			mouseButton    = MouseButtons::Left;
			mouseButtonDirection = ButtonDirections::Down;
			goto mouse_button_event;
		case WM_LBUTTONUP:
			mouseButton    = MouseButtons::Left;
			mouseButtonDirection = ButtonDirections::Up;
			goto mouse_button_event;
		case WM_LBUTTONDBLCLK:
			mouseButton    = MouseButtons::Left;
			mouseButtonDirection = ButtonDirections::DoubleClick;
			goto mouse_button_event;
		case WM_RBUTTONDOWN:
			mouseButton    = MouseButtons::Right;
			mouseButtonDirection = ButtonDirections::Down;
			goto mouse_button_event;
		case WM_RBUTTONUP:
			mouseButton    = MouseButtons::Right;
			mouseButtonDirection = ButtonDirections::Up;
			goto mouse_button_event;
		case WM_RBUTTONDBLCLK:
			mouseButton    = MouseButtons::Right;
			mouseButtonDirection = ButtonDirections::DoubleClick;
			goto mouse_button_event;
		case WM_MBUTTONDOWN:
			mouseButton    = MouseButtons::Middle;
			mouseButtonDirection = ButtonDirections::Down;
			goto mouse_button_event;
		case WM_MBUTTONUP:
			mouseButton    = MouseButtons::Middle;
			mouseButtonDirection = ButtonDirections::Up;
			goto mouse_button_event;
		case WM_MBUTTONDBLCLK:
			mouseButton    = MouseButtons::Middle;
			mouseButtonDirection = ButtonDirections::DoubleClick;
			goto mouse_button_event;
		case WM_XBUTTONDOWN:
			mouseButton    = static_cast<MouseButtons::value_t>(HIWORD(wparam) + 2);
			mouseButtonDirection = ButtonDirections::Down;
			goto mouse_button_event;
		case WM_XBUTTONUP:
			mouseButton    = static_cast<MouseButtons::value_t>(HIWORD(wparam) + 2);
			mouseButtonDirection = ButtonDirections::Up;
			goto mouse_button_event;
		case WM_XBUTTONDBLCLK:
			mouseButton    = static_cast<MouseButtons::value_t>(HIWORD(wparam) + 2);
			mouseButtonDirection = ButtonDirections::DoubleClick;
mouse_button_event:
			if(!OnMouseButton.is_empty())
			{
				int                        x           = static_cast<int>(GET_X_LPARAM(lparam));
				int                        y           = static_cast<int>(GET_Y_LPARAM(lparam));
				MouseVirtualKeyFlags::type virtualKeys = static_cast<MouseVirtualKeyFlags::type>(LOWORD(wparam));

				OnMouseButton.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					mouseButton, mouseButtonDirection, x, y, virtualKeys);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
		case WM_MOUSEHWHEEL:
			scrollDirection = ScrollDirections::Horizontal;
			goto mouse_wheel_event;
#endif
		case WM_MOUSEWHEEL:
			scrollDirection = ScrollDirections::Vertical;
mouse_wheel_event:
			if(!OnMouseWheel.is_empty())
			{
				unsigned fncKeys = static_cast<unsigned>(GET_KEYSTATE_WPARAM(wparam));
				int delta = static_cast<int>(GET_WHEEL_DELTA_WPARAM(wparam));
				int x = static_cast<int>(LOWORD(lparam));
				int y = static_cast<int>(HIWORD(lparam));

				OnMouseWheel.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					scrollDirection, delta, x, y, fncKeys);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_MOUSEHOVER:
			if(!OnMouseHover.is_empty())
			{
				unsigned fncKeys = static_cast<unsigned>(GET_KEYSTATE_WPARAM(wparam));
				int x = static_cast<int>(LOWORD(lparam));
				int y = static_cast<int>(HIWORD(lparam));

				OnMouseHover.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					 x, y, fncKeys);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

		case WM_MOUSELEAVE:
			if(!OnMouseLeave.is_empty())
			{
				OnMouseLeave.fire_accumulate([&](bool srv) { results.push_back(srv); });

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_PARENTNOTIFY:
			break;

		case WM_ENTERMENULOOP:
			break;

		case WM_EXITMENULOOP:
			break;

		case WM_NEXTMENU:
			break;

		case WM_SIZING:
			break;

		case WM_CAPTURECHANGED:
			break;

		case WM_MOVING:
			break;

		case WM_POWERBROADCAST:
			break;

		case WM_DEVICECHANGE:
			break;

		case WM_MDICREATE:
			break;

		case WM_MDIDESTROY:
			break;

		case WM_MDIACTIVATE:
			break;

		case WM_MDIRESTORE:
			break;

		case WM_MDINEXT:
			break;

		case WM_MDIMAXIMIZE:
			break;

		case WM_MDITILE:
			break;

		case WM_MDICASCADE:
			break;

		case WM_MDIICONARRANGE:
			break;

		case WM_MDIGETACTIVE:
			break;

		case WM_MDISETMENU:
			break;

		case WM_ENTERSIZEMOVE:
			break;

		case WM_EXITSIZEMOVE:
			break;

		case WM_DROPFILES:
			break;

		case WM_MDIREFRESHMENU:
			break;

#if _WIN32_WINNT >= _WIN32_WINNT_WIN8
		case WM_POINTERDEVICECHANGE:
			break;

		case WM_POINTERDEVICEINRANGE:
			break;

		case WM_POINTERDEVICEOUTOFRANGE:
			break;

#endif
#if _WIN32_WINNT >= _WIN32_WINNT_WIN7
		case WM_TOUCH:
			break;

#endif
#if _WIN32_WINNT >= _WIN32_WINNT_WIN8
		case WM_NCPOINTERUPDATE:
			break;

		case WM_NCPOINTERDOWN:
			break;

		case WM_NCPOINTERUP:
			break;

		case WM_POINTERUPDATE:
			break;

		case WM_POINTERDOWN:
			break;

		case WM_POINTERUP:
			break;

		case WM_POINTERENTER:
			break;

		case WM_POINTERLEAVE:
			break;

		case WM_POINTERACTIVATE:
			break;

		case WM_POINTERCAPTURECHANGED:
			break;

		case WM_TOUCHHITTESTING:
			break;

		case WM_POINTERWHEEL:
			break;

		case WM_POINTERHWHEEL:
			break;

#endif
		case WM_IME_SETCONTEXT:
			break;

		case WM_IME_NOTIFY:
			break;

		case WM_IME_CONTROL:
			break;

		case WM_IME_COMPOSITIONFULL:
			break;

		case WM_IME_SELECT:
			break;

		case WM_IME_CHAR:
			break;

		case WM_IME_REQUEST:
			break;

		case WM_IME_KEYDOWN:
			break;

		case WM_IME_KEYUP:
			break;

#endif
#if 0
		case WM_NCMOUSEHOVER:
			break;

		case WM_NCMOUSELEAVE:
			break;

		case WM_WTSSESSION_CHANGE:
			break;

#if _WIN32_WINNT >= _WIN32_WINNT_WIN7
		case WM_DPICHANGED:
			break;

#endif
		case WM_CUT:
			break;

		case WM_COPY:
			break;

		case WM_PASTE:
			break;

		case WM_CLEAR:
			break;

		case WM_UNDO:
			break;

		case WM_RENDERFORMAT:
			break;

		case WM_RENDERALLFORMATS:
			break;

		case WM_DESTROYCLIPBOARD:
			break;

		case WM_DRAWCLIPBOARD:
			break;

		case WM_PAINTCLIPBOARD:
			break;

		case WM_VSCROLLCLIPBOARD:
			break;

		case WM_SIZECLIPBOARD:
			break;

		case WM_ASKCBFORMATNAME:
			break;

		case WM_CHANGECBCHAIN:
			break;

		case WM_HSCROLLCLIPBOARD:
			break;

		case WM_QUERYNEWPALETTE:
			break;

		case WM_PALETTEISCHANGING:
			break;

		case WM_PALETTECHANGED:
			break;

#endif
		case WM_HOTKEY:
			if(!OnHotKey.is_empty())
			{
				int               id              = static_cast<int>(wparam);
				unsigned          virtualCode     = static_cast<int>(LOWORD(lparam));
				HotKeyFlags::type combinationWith = static_cast<HotKeyFlags::type>(HIWORD(lparam));

				OnHotKey.fire_accumulate(
					[&](bool srv) { results.push_back(srv); },
					 id, virtualCode, combinationWith);

				if(std::any_of(results.begin(), results.end(), [](bool result){ return result == true; }))
				{
					return 0;
				}
			}
			break;

#if 0
		case WM_PRINT:
			break;

		case WM_PRINTCLIENT:
			break;

		case WM_APPCOMMAND:
			break;

		case WM_THEMECHANGED:
			break;

		case WM_CLIPBOARDUPDATE:
			break;

#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
		case WM_DWMCOMPOSITIONCHANGED:
			break;

		case WM_DWMNCRENDERINGCHANGED:
			break;

		case WM_DWMCOLORIZATIONCOLORCHANGED:
			break;

		case WM_DWMWINDOWMAXIMIZEDCHANGE:
			break;

#endif
#if _WIN32_WINNT >= _WIN32_WINNT_WIN7
		case WM_DWMSENDICONICTHUMBNAIL:
			break;

		case WM_DWMSENDICONICLIVEPREVIEWBITMAP:
			break;

#endif
#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
		case WM_GETTITLEBARINFOEX:
			break;

#endif
#endif
	}

	return DefWindowProc(windowHandle_, message, wparam, lparam);
}

} /* namespace cxxwin */
