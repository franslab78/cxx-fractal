/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The window style flags.
 *
 * This file contains a class that represents window style flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/window_styles.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

static const char overlappedStr[] = /*  0: 0x00000000 00000000 */ "Overlapped";

/*----------------------------------------------------------------------------*/
static const char strs[][20] =
{
	/* 17: 0x00000000 00010000 */ "MaximisedBox",
	/* 18: 0x00000000 00020000 */ "MinimisedBox",
	/* 19: 0x00000000 00040000 */ "ThickFrame",
	/* 20: 0x00000000 00080000 */ "SystemMenu",
	/* 21: 0x00000000 00100000 */ "HorizontalScroll",
	/* 22: 0x00000000 00200000 */ "VerticalScroll",
	/* 23: 0x00000000 00400000 */ "DialogFrame",
	/* 24: 0x00000000 00800000 */ "Border",
	/* 25: 0x00000000 01000000 */ "Maximised",
	/* 26: 0x00000000 02000000 */ "ClipChildren",
	/* 27: 0x00000000 04000000 */ "ClipSiblings",
	/* 28: 0x00000000 08000000 */ "Disabled",
	/* 29: 0x00000000 10000000 */ "Visible",
	/* 20: 0x00000000 20000000 */ "Minimise",
	/* 31: 0x00000000 40000000 */ "Child",
	/* 32: 0x00000000 80000000 */ "Popup",

	/* 33: 0x00000001 00000000 */ "DialogModalFrame",
	/* 34: 0x00000002 00000000 */ "Invalid(34)",
	/* 35: 0x00000004 00000000 */ "NoParentNotify",
	/* 36: 0x00000008 00000000 */ "TopMost",
	/* 37: 0x00000010 00000000 */ "AcceptFiles",
	/* 38: 0x00000020 00000000 */ "Transparent",
	/* 39: 0x00000040 00000000 */ "MdiChild",
	/* 40: 0x00000080 00000000 */ "ToolWindow",
	/* 41: 0x00000100 00000000 */ "WindowEdge",
	/* 42: 0x00000200 00000000 */ "ClientEdge",
	/* 43: 0x00000400 00000000 */ "ContextHelp",
	/* 44: 0x00000800 00000000 */ "Invalid(44)",
	/* 45: 0x00001000 00000000 */ "Right",
	/* 46: 0x00002000 00000000 */ "RightToLeftReading",
	/* 47: 0x00004000 00000000 */ "LeftScrollBar",
	/* 48: 0x00008000 00000000 */ "Invalid(48)",
	/* 49: 0x00010000 00000000 */ "ControlParent",
	/* 50: 0x00020000 00000000 */ "StaticEdge",
	/* 51: 0x00040000 00000000 */ "ApplicationWindow",
	/* 52: 0x00080000 00000000 */ "Layered",
	/* 53: 0x00100000 00000000 */ "NoInheritLayout",
	/* 54: 0x00200000 00000000 */ "NoRedirectionBitmap",
	/* 55: 0x00400000 00000000 */ "LayoutRightToLeft",
	/* 56: 0x00800000 00000000 */ "Invalid(56)",
	/* 57: 0x01000000 00000000 */ "Invalid(57)",
	/* 58: 0x02000000 00000000 */ "Composited",
	/* 59: 0x04000000 00000000 */ "Invalid(59)",
	/* 60: 0x08000000 00000000 */ "NoActivate",
	/* 61: 0x10000000 00000000 */ "Invalid(61)",
	/* 62: 0x20000000 00000000 */ "Invalid(62)",
	/* 63: 0x40000000 00000000 */ "Invalid(63)",
	/* 64: 0x80000000 00000000 */ "Invalid(64)",
};

#define FLAGS_FIRST 18
#define FLAGS_LAST  60
#define FLAGS_MAX   (sizeof(WindowStyleFlags::type) * 8)

/*----------------------------------------------------------------------------*/
std::string WindowStyleFlags::ToString(type flags)
{
	return cxxwin::FlagsToString(flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		overlappedStr, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
WindowStyleFlags::type WindowStyleFlags::FromString(
	const std::string_view str)
{
	return static_cast<WindowStyleFlags::type>(cxxwin::FlagsFromString(
		str, FLAGS_FIRST, FLAGS_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0])));
}

/*----------------------------------------------------------------------------*/
std::ostream& WindowStyleFlags::ToStream(std::ostream & os, type flags)
{
	return cxxwin::FlagsToStream(os, flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
std::istream& WindowStyleFlags::FromStream(
	std::istream & is,
	type &         flags)
{
	uint64_t uflags;
	cxxwin::FlagsFromStream(
		is, uflags, FLAGS_FIRST, FLAGS_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	flags = static_cast<WindowStyleFlags::type>(uflags);
	return is;
}

} /* namespace cxxwin */
