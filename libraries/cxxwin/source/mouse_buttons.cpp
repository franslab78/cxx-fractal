/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The mouse buttons enumerate.
 *
 * This file contains a class that represents mouse buttons enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/mouse_buttons.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][7] =
{
	/*  0 */ "Left",
	/*  1 */ "Right",
	/*  2 */ "Middle",
	/*  3 */ "X1",
	/*  4 */ "X2"
};

#define ENUM_FIRST 0
#define ENUM_LAST  static_cast<uint64_t>(sizeof(strs) / sizeof(strs[0]))

/*----------------------------------------------------------------------------*/
char const * MouseButtons::ToString(MouseButtons value)
{
	return EnumToString(static_cast<uint64_t>(value.value_),
		ENUM_FIRST, ENUM_LAST, reinterpret_cast<char const *>(strs), sizeof(strs[0]), invalidEnumString);

}

/*----------------------------------------------------------------------------*/
MouseButtons MouseButtons::FromString(
	const std::string_view str)
{
	return MouseButtons(static_cast<MouseButtons::value_t>(cxxwin::EnumFromString(
		str, ENUM_FIRST, ENUM_LAST, reinterpret_cast<char const *>(strs), sizeof(strs[0]))));
}

/*----------------------------------------------------------------------------*/
std::ostream& MouseButtons::ToStream(std::ostream & os, MouseButtons value)
{
	return cxxwin::EnumToStream(os, static_cast<uint64_t>(value), ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidEnumString));
}

/*----------------------------------------------------------------------------*/
std::istream& MouseButtons::FromStream(
	std::istream & is,
	MouseButtons & value)
{
	uint64_t uvalue;
	cxxwin::EnumFromStream(
		is, uvalue, ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	value = static_cast<MouseButtons::value_t>(uvalue);
	return is;
}

} /* namespace cxxwin */
