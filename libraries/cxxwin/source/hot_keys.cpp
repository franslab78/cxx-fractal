/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The hot key flags.
 *
 * This file contains a class that represents hot key flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-12
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/hot_keys.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][8] =
{
	/*  1: 0x00000001 */ "Alt",
	/*  2: 0x00000002 */ "Control",
	/*  3: 0x00000004 */ "Shift",
	/*  4: 0x00000008 */ "Win"
};

#define FLAGS_FIRST 1
#define FLAGS_LAST  4
#define FLAGS_MAX   (sizeof(HotKeyFlags::type) * 8)

/*----------------------------------------------------------------------------*/
std::string HotKeyFlags::ToString(type flags)
{
	return cxxwin::FlagsToString(flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
HotKeyFlags::type HotKeyFlags::FromString(
	const std::string_view str)
{
	return static_cast<HotKeyFlags::type>(cxxwin::FlagsFromString(str, 
		FLAGS_FIRST, FLAGS_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0])));
}

/*----------------------------------------------------------------------------*/
std::ostream& HotKeyFlags::ToStream(std::ostream & os, type flags)
{
	return cxxwin::FlagsToStream(os, flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
std::istream& HotKeyFlags::FromStream(
	std::istream & is,
	type &         flags)
{
	uint64_t uflags;
	cxxwin::FlagsFromStream(
		is, uflags, FLAGS_FIRST, FLAGS_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	flags = static_cast<HotKeyFlags::type>(uflags);
	return is;
}

} /* namespace cxxwin */
