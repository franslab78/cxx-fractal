/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The strings.
 *
 * This file contains the strings.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/strings.hpp"

namespace cxxwin
{

const char noneString[] = "None";
const char orString[]  = " | ";
const char invalidEnumString[] = "Invalid";

const char invalidFlagStrings[64][12] =
{
	/*  1: 0x00000000 00000001 */ "Invalid(1)",
	/*  2: 0x00000000 00000002 */ "Invalid(2)",
	/*  3: 0x00000000 00000004 */ "Invalid(3)",
	/*  4: 0x00000000 00000008 */ "Invalid(4)",
	/*  5: 0x00000000 00000010 */ "Invalid(5)",
	/*  6: 0x00000000 00000020 */ "Invalid(6)",
	/*  7: 0x00000000 00000040 */ "Invalid(7)",
	/*  8: 0x00000000 00000080 */ "Invalid(8)",
	/*  9: 0x00000000 00000100 */ "Invalid(9)",
	/* 10: 0x00000000 00000200 */ "Invalid(10)",
	/* 11: 0x00000000 00000400 */ "Invalid(11)",
	/* 12: 0x00000000 00000800 */ "Invalid(12)",
	/* 13: 0x00000000 00001000 */ "Invalid(13)",
	/* 14: 0x00000000 00002000 */ "Invalid(14)",
	/* 15: 0x00000000 00004000 */ "Invalid(15)",
	/* 16: 0x00000000 00008000 */ "Invalid(16)",
	/* 17: 0x00000000 00010000 */ "Invalid(17)",
	/* 18: 0x00000000 00020000 */ "Invalid(18)",
	/* 19: 0x00000000 00040000 */ "Invalid(18)",
	/* 20: 0x00000000 00080000 */ "Invalid(20)",
	/* 20: 0x00000000 00100000 */ "Invalid(21)",
	/* 20: 0x00000000 00200000 */ "Invalid(22)",
	/* 20: 0x00000000 00400000 */ "Invalid(23)",
	/* 20: 0x00000000 00800000 */ "Invalid(24)",
	/* 20: 0x00000000 01000000 */ "Invalid(25)",
	/* 20: 0x00000000 02000000 */ "Invalid(26)",
	/* 20: 0x00000000 04000000 */ "Invalid(27)",
	/* 20: 0x00000000 08000000 */ "Invalid(28)",
	/* 20: 0x00000000 10000000 */ "Invalid(29)",
	/* 30: 0x00000000 20000000 */ "Invalid(30)",
	/* 31: 0x00000000 40000000 */ "Invalid(31)",
	/* 32: 0x00000000 80000000 */ "Invalid(32)",

	/* 33: 0x00000001 00000000 */ "Invalid(33)",
	/* 34: 0x00000002 00000000 */ "Invalid(34)",
	/* 35: 0x00000004 00000000 */ "Invalid(35)",
	/* 36: 0x00000008 00000000 */ "Invalid(36)",
	/* 37: 0x00000010 00000000 */ "Invalid(37)",
	/* 38: 0x00000020 00000000 */ "Invalid(38)",
	/* 39: 0x00000040 00000000 */ "Invalid(39)",
	/* 40: 0x00000080 00000000 */ "Invalid(40)",
	/* 41: 0x00000100 00000000 */ "Invalid(41)",
	/* 42: 0x00000200 00000000 */ "Invalid(42)",
	/* 43: 0x00000400 00000000 */ "Invalid(43)",
	/* 44: 0x00000800 00000000 */ "Invalid(44)",
	/* 45: 0x00001000 00000000 */ "Invalid(45)",
	/* 46: 0x00002000 00000000 */ "Invalid(46)",
	/* 47: 0x00004000 00000000 */ "Invalid(47)",
	/* 48: 0x00008000 00000000 */ "Invalid(48)",
	/* 49: 0x00010000 00000000 */ "Invalid(49)",
	/* 50: 0x00020000 00000000 */ "Invalid(50)",
	/* 51: 0x00040000 00000000 */ "Invalid(51)",
	/* 52: 0x00080000 00000000 */ "Invalid(52)",
	/* 53: 0x00100000 00000000 */ "Invalid(53)",
	/* 54: 0x00200000 00000000 */ "Invalid(54)",
	/* 55: 0x00400000 00000000 */ "Invalid(55)",
	/* 56: 0x00800000 00000000 */ "Invalid(56)",
	/* 57: 0x01000000 00000000 */ "Invalid(57)",
	/* 58: 0x02000000 00000000 */ "Invalid(58)",
	/* 59: 0x04000000 00000000 */ "Invalid(59)",
	/* 60: 0x08000000 00000000 */ "Invalid(60)",
	/* 61: 0x10000000 00000000 */ "Invalid(61)",
	/* 62: 0x20000000 00000000 */ "Invalid(62)",
	/* 63: 0x40000000 00000000 */ "Invalid(63)",
	/* 64: 0x80000000 00000000 */ "Invalid(64)",
};

/*----------------------------------------------------------------------------*/
std::string FlagsToString(
	uint64_t     flags,
	unsigned     first,
	unsigned     last,
	unsigned     max,
	char const * nullstr,
	char const * orstr,
	char const * flagstr,
	size_t       flagsz,
	char const * invalidstr,
	size_t       invalidsz)
{
	std::string ret;
	if(0 == flags)
	{
		ret += nullstr;
	}
	else
	{
		uint64_t flag = 0x1;
		unsigned idx  = 0;
		for(; idx < first; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(!ret.empty())
				{
					ret += orstr;
				}
				ret        += invalidstr;
				invalidstr += invalidsz;
			}
		}
		for(; idx < last; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(!ret.empty())
				{
					ret += orstr;
				}
				ret        += flagstr;
				flagstr    += flagsz;
				invalidstr += invalidsz;
			}
		}
		for(; idx < max; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(!ret.empty())
				{
					ret += orstr;
				}
				ret += invalidstr;
				invalidstr += invalidsz;
			}
		}
	}

	return ret;
}

/*----------------------------------------------------------------------------*/
std::ostream& FlagsToStream(
	std::ostream & os,
	uint64_t       flags,
	unsigned       first,
	unsigned       last,
	unsigned       max,
	char const *   nullstr,
	char const *   orstr,
	char const *   flagstr,
	size_t         flagsz,
	char const *   invalidstr,
	size_t         invalidsz)
{
	if(0 == flags)
	{
		os << nullstr;
	}
	else
	{
		bool     ored = false;
		uint64_t flag = 0x1;
		unsigned idx  = 0;
		for(; idx < first; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(ored)
				{
					os << orstr;
				}
				os << invalidstr;
				invalidstr += invalidsz;
				ored = true;
			}
		}
		for(; idx < last; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(ored)
				{
					os << orstr;
				}
				os << flagstr;
				flagstr    += flagsz;
				invalidstr += invalidsz;
				ored = true;
			}
		}
		for(; idx < max; idx++, flag <<= 1)
		{
			if(0 != (flags & flag))
			{
				if(ored)
				{
					os << orstr;
				}
				os << invalidstr;
				invalidstr += invalidsz;
				ored = true;
			}
		}
	}

	return os;
}

/*----------------------------------------------------------------------------*/
uint64_t FlagsFromString(
	const std::string_view str,
	unsigned               first,
	unsigned               last,
	char const *           flagstr,
	size_t                 flagsz)
{
	static const char delimiters[] = " |";

	char const * const flagend = flagstr + (last - first) * flagsz;
	uint64_t flags = 0;

	/* Tokenize */
	std::string_view::size_type start = str.find_first_not_of(delimiters);
	std::string_view::size_type end   = start;
	while (start != std::string::npos)
	{
		// Find next occurrence of delimiter
		end = str.find(delimiters, start);
		//end = str.find_first_of(delimiter, start);

		/* Compare the token to the strings. */
		bool     found = false;
		uint64_t flag  = 0x1 << first;
		char const * s = flagstr;
		while(s < flagend)
		{
			if(str.compare(start, end-start, s) == 0)
			{
				found = true;
				flags |= flag;
				break;
			}
			flag <<= 1;
			s     += flagsz;
		}

		// Skip all occurrences of the delimiter to find new start
		start = str.find_first_not_of(delimiters, end);
	}

	return flags;
}

/*----------------------------------------------------------------------------*/
std::istream& FlagsFromStream(
	std::istream & is,
	uint64_t &     flags,
	unsigned       first,
	unsigned       last,
	char const *   flagstr,
	size_t         flagsz)
{
	std::string token;
	while(is >> token)
	{
		uint64_t flag = 0x1 << first;
		for(size_t idx = first; idx < last; idx++, flag <<= 1)
		{
			if(token == flagstr)
			{
				flags |= flag;
			}
			flagstr += flagsz;
		}
	}
	return is;
}

/*----------------------------------------------------------------------------*/
char const * EnumToString(
	uint64_t     value,
	uint64_t     first,
	uint64_t     last,
	char const * valuestr,
	size_t       valuesz,
	char const * invalidstr)
{
	if((value < first) || (value >= last))
	{
		return invalidstr;
	}
	unsigned idx = (value / valuesz) - first;
	return &valuestr[idx];
}

/*----------------------------------------------------------------------------*/
uint64_t EnumFromString(
	const std::string_view str,
	uint64_t               first,
	uint64_t               last,
	char const *           valuestr,
	size_t                 valuesz)
{
	char const * const valueend = valuestr + (last - first) * valuesz;
	uint64_t flags = 0;

	/* Compare the token to the strings. */
	bool     found = false;
	uint64_t value = first;
	char const * s = valuestr;
	while(s < valueend)
	{
		if(str.compare(s) == 0)
		{
			found = true;
			break;
		}
		value += 1;
		s     += valuesz;
	}

	return value;
}

/*----------------------------------------------------------------------------*/
std::ostream& EnumToStream(
	std::ostream & os,
	uint64_t       value,
	uint64_t       first,
	uint64_t       last,
	char const *   valuestr,
	size_t         valuesz,
	char const *   invalidstr)
{
	if((value < first) || (value >= last))
	{
		os << invalidstr;
		return os;
	}
	unsigned idx = (value / valuesz) - first;
	os << valuestr[idx];
	return os;
}

/*----------------------------------------------------------------------------*/
std::istream& EnumFromStream(
	std::istream & is,
	uint64_t &     value,
	uint64_t       first,
	uint64_t       last,
	char const *   valuestr,
	size_t         valuesz)
{
	std::string str;
	is >> str;

	value = EnumFromString(str, first, last, valuestr, valuesz);

	return is;
}

} /* namespace cxxwin */
