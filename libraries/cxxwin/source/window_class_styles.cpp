/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The window class style flags.
 *
 * This file contains a class that represents window class style flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/window_class_styles.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][20] =
{
	/*  1: 0x00000001 */ "VerticalRedraw",
	/*  2: 0x00000002 */ "HorizontalRedraw",
	/*  3: 0x00000004 */ "Invalid(3)",
	/*  4: 0x00000008 */ "DoubleClicks",
	/*  5: 0x00000010 */ "Invalid(5)",
	/*  6: 0x00000020 */ "OwnDeviceContext",
	/*  7: 0x00000040 */ "SharedDeviceContext",
	/*  8: 0x00000080 */ "ParentDeviceContext",
	/*  9: 0x00000100 */ "Invalid(9)",
	/* 10: 0x00000200 */ "NoClose",
	/* 11: 0x00000400 */ "Invalid(11)",
	/* 12: 0x00000800 */ "SaveAsBitmap",
	/* 13: 0x00001000 */ "ByteAlignClient",
	/* 14: 0x00002000 */ "ByteAlignWindow",
	/* 15: 0x00004000 */ "Global",
	/* 16: 0x00008000 */ "Invalid(16)",
	/* 17: 0x00010000 */ "Invalid(17)",
	/* 18: 0x00020000 */ "EnableDropShadow"
};

#define FLAGS_FIRST 1
#define FLAGS_LAST  18
#define FLAGS_MAX   (sizeof(WindowClassStyleFlags::type) * 8)

/*----------------------------------------------------------------------------*/
std::string WindowClassStyleFlags::ToString(type flags)
{
	return cxxwin::FlagsToString(flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
WindowClassStyleFlags::type WindowClassStyleFlags::FromString(
	const std::string_view str)
{
	return static_cast<WindowClassStyleFlags::type>(cxxwin::FlagsFromString(
		str, FLAGS_FIRST, FLAGS_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0])));
}

/*----------------------------------------------------------------------------*/
std::ostream& WindowClassStyleFlags::ToStream(std::ostream & os, type flags)
{
	return cxxwin::FlagsToStream(os, flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
std::istream& WindowClassStyleFlags::FromStream(
	std::istream & is,
	type &         flags)
{
	uint64_t uflags;
	cxxwin::FlagsFromStream(
		is, uflags, FLAGS_FIRST, FLAGS_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	flags = static_cast<WindowClassStyleFlags::type>(uflags);
	return is;
}

} /* namespace cxxwin */
