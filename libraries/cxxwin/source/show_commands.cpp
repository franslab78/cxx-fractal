/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The show command enumerate.
 *
 * This file contains a class that represents show command enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-17
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/show_commands.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][17] =
{
	/*  0 */ "Hide",
	/*  1 */ "Normal",
	/*  2 */ "Minimised",
	/*  3 */ "Maximised",
	/*  4 */ "NoActive",
	/*  5 */ "Show",
	/*  6 */ "MinimiseNextTop",
	/*  7 */ "MinimiseNoActive",
	/*  8 */ "ShowNoActive",
	/*  9 */ "Restore",
	/* 10 */ "Default",
	/* 11 */ "ForceMinimised"
};

#define ENUM_FIRST 0
#define ENUM_LAST  static_cast<uint64_t>(sizeof(strs) / sizeof(strs[0]))

/*----------------------------------------------------------------------------*/
char const * ShowCommands::ToString(ShowCommands value)
{
	return EnumToString(static_cast<uint64_t>(value.value_),
		ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]), 
		invalidEnumString);
}

/*----------------------------------------------------------------------------*/
ShowCommands ShowCommands::FromString(
	const std::string_view str)
{
	return ShowCommands(static_cast<ShowCommands::value_t>(cxxwin::EnumFromString(
		str, ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]))));
}

/*----------------------------------------------------------------------------*/
std::ostream& ShowCommands::ToStream(std::ostream & os, ShowCommands value)
{
	return cxxwin::EnumToStream(os, static_cast<uint64_t>(value), 
		ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidEnumString));
}

/*----------------------------------------------------------------------------*/
std::istream& ShowCommands::FromStream(
	std::istream & is,
	ShowCommands & value)
{
	uint64_t uvalue;
	cxxwin::EnumFromStream(
		is, uvalue, ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	value = static_cast<ShowCommands::value_t>(uvalue);
	return is;
}

} /* namespace cxxwin */
