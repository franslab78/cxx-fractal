/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The mouse virtual key flags.
 *
 * This file contains a class that represents mouse virtual key flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-12
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/key_states.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][16] =
{
	/* 24: 0x01000000 */ "IsExtended",
	/* 25: 0x02000000 */ "Invalid(25)",
	/* 26: 0x04000000 */ "Invalid(26)",
	/* 27: 0x08000000 */ "Invalid(27)",
	/* 28: 0x10000000 */ "Invalid(28)",
	/* 29: 0x20000000 */ "ContextCode",
	/* 30: 0x40000000 */ "PreviousState",
	/* 31: 0x80000000 */ "TransitionState"
};

#define FLAGS_FIRST 24
#define FLAGS_LAST  31
#define FLAGS_MAX   (sizeof(KeyStateFlags::type) * 8)

/*----------------------------------------------------------------------------*/
std::string KeyStateFlags::ToString(type flags)
{
	return cxxwin::FlagsToString(flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
KeyStateFlags::type KeyStateFlags::FromString(
	const std::string_view str)
{
	return static_cast<KeyStateFlags::type>(cxxwin::FlagsFromString(str, 
		FLAGS_FIRST, FLAGS_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0])));
}

/*----------------------------------------------------------------------------*/
std::ostream& KeyStateFlags::ToStream(std::ostream & os, type flags)
{
	return cxxwin::FlagsToStream(os, flags, FLAGS_FIRST, FLAGS_LAST, FLAGS_MAX,
		noneString, orString,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidFlagStrings), sizeof(invalidFlagStrings[0]));
}

/*----------------------------------------------------------------------------*/
std::istream& KeyStateFlags::FromStream(
	std::istream & is,
	type &         flags)
{
	uint64_t uflags;
	cxxwin::FlagsFromStream(
		is, uflags, FLAGS_FIRST, FLAGS_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	flags = static_cast<KeyStateFlags::type>(uflags);
	return is;
}

} /* namespace cxxwin */
