/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The key directions enumerate.
 *
 * This file contains a class that represents key directions enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/key_directions.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][16] =
{
	/*  0 */ "Down",
	/*  1 */ "Up",
	/*  2 */ "SystemDown",
	/*  3 */ "SystemUp",
	/*  4 */ "Character",
	/*  5 */ "SystemCharacter"
};

#define ENUM_FIRST 0
#define ENUM_LAST  static_cast<uint64_t>(sizeof(strs) / sizeof(strs[0]))

/*----------------------------------------------------------------------------*/
char const * KeyDirections::ToString(KeyDirections value)
{
	return EnumToString(static_cast<uint64_t>(value.value_),
		ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]), 
		invalidEnumString);
}

/*----------------------------------------------------------------------------*/
KeyDirections KeyDirections::FromString(
	const std::string_view str)
{
	return KeyDirections(static_cast<KeyDirections::value_t>(cxxwin::EnumFromString(str, 
		ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]))));
}

/*----------------------------------------------------------------------------*/
std::ostream& KeyDirections::ToStream(std::ostream & os, KeyDirections value)
{
	return cxxwin::EnumToStream(os, static_cast<uint64_t>(value), 
		ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidEnumString));
}

/*----------------------------------------------------------------------------*/
std::istream& KeyDirections::FromStream(
	std::istream & is,
	KeyDirections & value)
{
	uint64_t uvalue;
	cxxwin::EnumFromStream(
		is, uvalue, ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	value = static_cast<KeyDirections::value_t>(uvalue);
	return is;
}

} /* namespace cxxwin */
