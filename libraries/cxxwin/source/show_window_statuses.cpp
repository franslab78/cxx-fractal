/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The show windows statuses enumerate.
 *
 * This file contains a class that represents show windows statuses enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#include "cxxwin/show_window_statuses.hpp"
#include "cxxwin/strings.hpp"

namespace cxxwin
{

/*----------------------------------------------------------------------------*/
static const char strs[][14] =
{
	/*  0 */ "Called",
	/*  1 */ "Closing",
	/*  2 */ "OtherZoom",
	/*  3 */ "ParentOpening",
	/*  4 */ "OtherUnzoom"
};

#define ENUM_FIRST 0
#define ENUM_LAST  static_cast<uint64_t>(sizeof(strs) / sizeof(strs[0]))

/*----------------------------------------------------------------------------*/
char const * ShowWindowStatuses::ToString(ShowWindowStatuses value)
{
	return EnumToString(static_cast<uint64_t>(value.value_),
		ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]), 
		invalidEnumString);
}

/*----------------------------------------------------------------------------*/
ShowWindowStatuses ShowWindowStatuses::FromString(
	const std::string_view str)
{
	return ShowWindowStatuses(static_cast<ShowWindowStatuses::value_t>(cxxwin::EnumFromString(
		str, ENUM_FIRST, ENUM_LAST, 
		reinterpret_cast<char const *>(strs), sizeof(strs[0]))));
}

/*----------------------------------------------------------------------------*/
std::ostream& ShowWindowStatuses::ToStream(
	std::ostream &     os, 
	ShowWindowStatuses value)
{
	return cxxwin::EnumToStream(os, static_cast<uint64_t>(value), 
		ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]),
		reinterpret_cast<char const *>(invalidEnumString));
}

/*----------------------------------------------------------------------------*/
std::istream& ShowWindowStatuses::FromStream(
	std::istream &       is,
	ShowWindowStatuses & value)
{
	uint64_t uvalue;
	cxxwin::EnumFromStream(
		is, uvalue, ENUM_FIRST, ENUM_LAST,
		reinterpret_cast<char const *>(strs), sizeof(strs[0]));
	value = static_cast<ShowWindowStatuses::value_t>(uvalue);
	return is;
}

} /* namespace cxxwin */
