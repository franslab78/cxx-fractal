/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The show command enumerate.
 *
 * This file contains a class that represents show command enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-17
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef HB633C78E_080A_49AD_875C_49D75EF3BC98
#define HB633C78E_080A_49AD_875C_49D75EF3BC98

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Show commands.
 *
 * Controls how the window is to be shown.
 *
 * https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
 */
class ShowCommands
{
public:
	enum value_t
	{
		/*! @brief Hides the window and activates another window.
		 *
		 * Hides the window and activates another window.
		 */
		Hide = SW_HIDE,
		/*! @brief Activates and displays a window.
		 *
		 * Activates and displays a window. If the window is minimised or
		 * maximised, the system restores it to its original size and position. An
		 * application should specify this flag when displaying the window for the
		 * first time.
		 */
		Normal = SW_SHOWNORMAL,
		/*! @brief Minimises the specified window.
		 *
		 * Activates the window and displays it as a minimised window.
		 */
		Minimised = SW_SHOWMINIMIZED,
		/*! @brief Maximises the specified window.
		 *
		 * Activates the window and displays it as a maximised window.
		 */
		Maximised = SW_SHOWMAXIMIZED,
		/*! @brief Displays a window in its most recent size and position.
		 *
		 * Displays a window in its most recent size and position. This value is
		 * similar to SW_SHOWNORMAL, except that the window is not activated.
		 */
		NoActive = SW_SHOWNOACTIVATE,
		/*! @brief Activates the window and displays it in its current size and
		 * position.
		 *
		 * Activates the window and displays it in its current size and position.
		 */
		Show = SW_SHOW,
		/*! @brief Minimises the specified window.
		 *
		 * Minimises the specified window and activates the next top-level window
		 * in the Z order.
		 */
		MinimiseNextTop = SW_MINIMIZE,
		/*! @brief Displays the window as a minimised window.
		 *
		 * Displays the window as a minimised window. This value is similar to
		 * SW_SHOWMINIMIZED, except the window is not activated.
		 */
		MinimiseNoActive = SW_SHOWMINNOACTIVE,
		/*! @brief Displays the window in its current size and position.
		 *
		 * Displays the window in its current size and position. This value is
		 * similar to SW_SHOW, except that the window is not activated.
		 */
		ShowNoActive = SW_SHOWNA,
		/*! @brief Activates and displays the window.
		 *
		 * Activates and displays the window. If the window is minimised or
		 * maximised, the system restores it to its original size and position. An
		 * application should specify this flag when restoring a minimised window.
		 */
		Restore = SW_RESTORE,
		/*! @brief
		 *
		 * Sets the show state based on the SW_ value specified in the STARTUPINFO
		 * structure passed to the CreateProcess function by the program that
		 * started the application.
		 */
		Default = SW_SHOWDEFAULT,
		/*! @brief Minimises a window.
		 *
		 * Minimises a window, even if the thread that owns the window is not
		 * responding. This flag should only be used when minimising windows from
		 * a different thread.
		 */
		ForceMinimised = SW_FORCEMINIMIZE
	};

	ShowCommands() = default;
	constexpr ShowCommands(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(ShowCommands a) const { return value_ == a.value_; }
	constexpr bool operator!=(ShowCommands a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, ShowCommands a) { return value == a.value_; }
	friend bool operator!=(value_t value, ShowCommands a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(ShowCommands value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string
	 *
	 * @return The enumerate literal.
	 */
	static ShowCommands FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, ShowCommands value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, ShowCommands & value);

private:
	value_t value_ = Normal;
};

} /* namespace cxxwin*/

#endif /* HB633C78E_080A_49AD_875C_49D75EF3BC98 */
