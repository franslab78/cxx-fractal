/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The mouse virtual key flags.
 *
 * This file contains a class that represents mouse virtual key flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-12
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H5CCA199E_74FC_452C_A738_B84F3A216169
#define H5CCA199E_74FC_452C_A738_B84F3A216169

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief The mouse virtual key flags.
 *
 * The mouse virtual key flags.
 *
 * https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-lbuttondown
 */
struct MouseVirtualKeyFlags
{
	/*! @brief The type used for the flags. */
	using type = uint32_t;

	/*! @brief No mouse virtual key. */
	static constexpr type None = 0;

	/*! @brief The left mouse button is down.
	 *
	 * The left mouse button is down.
	 */
	static constexpr type LeftButton = MK_LBUTTON;
	/*! @brief The right mouse button is down.
	 *
	 * The right mouse button is down.
	 */
	static constexpr type RightButton = MK_RBUTTON;
	/*! @brief The SHIFT key is down.
	 *
	 * The SHIFT key is down.
	 */
	static constexpr type Shift = MK_SHIFT;
	/*! @brief The CTRL key is down.
	 *
	 * The CTRL key is down.
	 */
	static constexpr type Control = MK_CONTROL;
	/*! @brief The middle mouse button is down.
	 *
	 * The middle mouse button is down.
	 */
	static constexpr type MiddleButton = MK_MBUTTON;
	/*! @brief The first X button is down.
	 *
	 * The first X button is down.
	 */
	static constexpr type X1Button = MK_XBUTTON1;
	/*! @brief The second X button is down.
	 *
	 * The second X button is down.
	 */
	static constexpr type X2Button = MK_XBUTTON2;

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the flags to a human readable string.
	 *
	 * @param[in] flags The flags.
	 *
	 * @return The human readable flags string.
	 */
	static std::string ToString(type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in] str The flags string.
	 *
	 * @return The flags.
	 */
	static type FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the flags to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     flags The flags.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    flags The flags.
	 *
	 * @return The input flags.
	 */
	static std::istream& FromStream(std::istream & is, type & flags);
};

} /* namespace cxxwin */

#endif /* H5CCA199E_74FC_452C_A738_B84F3A216169 */
