/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The common string operations.
 *
 * This file contains the common string operations.
 *
 * @author Frans Labuschagne
 * @date   2020-03-10
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H11C9F1DB_7E5F_482B_B48E_FA6B60C677D6
#define H11C9F1DB_7E5F_482B_B48E_FA6B60C677D6

#include <string>
#include <string_view>
#include <cstdint>
#include <iostream>

namespace cxxwin {

extern const char invalidFlagStrings[64][12]; /* "Invalid(idx)" */
extern const char invalidEnumString[12]; /* "Invalid" */
extern const char noneString[5]; /* "None" */
extern const char orString[4]; /* " | " */

/*----------------------------------------------------------------------------*/
/*! @brief Convert flags to string.
 *
 * This operation converts flags into human readable string. 
 * 
 * @param[in] flags      The flags to convert.
 * @param[in] first      The index of the first flag of the set of flags.
 * @param[in] last       The index of the last flag of the set of flags.
 * @param[in] max        The index of the maximum flags of the set of flags.
 * @param[in] nullstr    Null terminated character string that is returned when
 *                       the flags is zero.
 * @param[in] orstr      Null terminated character string that is used between 
 *                       flags.
 * @param[in] flagstr    The array of null terminated strings that represent the 
 *                       flags.
 * @param[in] flagsz     The size of a single element in the flags string array.
 * @param[in] invalidstr The array of null terminated strings that represent the
 *                       flags that is not part of the flags set.
 * @param[in] invalidsz  The size of a single element in the invalid flags 
 *                       string array.
 * 
 * @return The human readable string of all the flags that are set.
 */
extern std::string FlagsToString(
	uint64_t     flags,
	unsigned     first,
	unsigned     last,
	unsigned     max,
	char const * nullstr,
	char const * orstr,
	char const * flagstr,
	size_t       flagsz,
	char const * invalidstr,
	size_t       invalidsz);

/*----------------------------------------------------------------------------*/
/*! @brief Convert human readable string to flags.
 *
 * This operation converts human readable string to flags.
 * 
 * @param[in] str     The human readable string.
 * @param[in] first   The index of the first flag of the set of flags.
 * @param[in] last    The index of the last flag of the set of flags.
 * @param[in] flagstr The array of null terminated strings that represent the 
 *                    flags.
 * @param[in] flagsz  The size of a single element in the flags string array.
 * 
 * @return The flags.
 */
extern uint64_t FlagsFromString(
	const std::string_view str,
	unsigned               first,
	unsigned               last,
	char const *           flagstr,
	size_t                 flagsz);

/*----------------------------------------------------------------------------*/
/*! @brief Convert flags to stream.
 *
 * This operation converts flags into human readable stream. 
 * 
 * @param[out] os         The output stream.
 * @param[in]  flags      The flags to convert.
 * @param[in]  first      The index of the first flag of the set of flags.
 * @param[in]  last       The index of the last flag of the set of flags.
 * @param[in]  max        The index of the maximum flags of the set of flags.
 * @param[in]  nullstr    Null terminated character string that is returned when
 *                        the flags is zero.
 * @param[in]  orstr      Null terminated character string that is used between 
 *                        flags.
 * @param[in]  flagstr    The array of null terminated strings that represent 
 *                        the flags.
 * @param[in]  flagsz     The size of a single element in the flags string 
 *                        array.
 * @param[in]  invalidstr The array of null terminated strings that represent
 *                        the flags that is not part of the flags set.
 * @param[in]  invalidsz  The size of a single element in the invalid flags 
 *                        string array.
 * 
 * @return The human readable string of all the flags that are set.
 */
extern std::ostream& FlagsToStream(
	std::ostream & os,
	uint64_t       flags,
	unsigned       first,
	unsigned       last,
	unsigned       max,
	char const *   nullstr,
	char const *   orstr,
	char const *   flagstr,
	size_t         flagsz,
	char const *   invalidstr,
	size_t         invalidsz);

/*----------------------------------------------------------------------------*/
/*! @brief Convert human readable stream to flags.
 *
 * This operation converts human readable stream to flags.
 * 
 * @param[in] is      The input stream.
 * @param[in] str     The human readable string.
 * @param[in] first   The index of the first flag of the set of flags.
 * @param[in] last    The index of the last flag of the set of flags.
 * @param[in] flagstr The array of null terminated strings that represent the 
 *                    flags.
 * @param[in] flagsz  The size of a single element in the flags string array.
 * 
 * @return The flags.
 */
extern std::istream& FlagsFromStream(
	std::istream & is,
	uint64_t &     flags,
	unsigned       first,
	unsigned       last,
	char const *   flagstr,
	size_t         flagsz);

/*----------------------------------------------------------------------------*/
/*! @brief Convert enumerate literal to string.
 *
 * This operation converts enumerate literal value into human readable string. 
 * 
 * @param[in] value      The enumerate literal value to convert.
 * @param[in] first      The first enumerate literal value.
 * @param[in] last       The last enumerate literal value.
 * @param[in] valuestr   The array of null terminated strings that represent the 
 *                       enumerate literals.
 * @param[in] valuesz    The size of a single element in the enumerate literal 
 *                       string array.
 * @param[in] invalidstr The null terminated string that represent the
 *                       enumerate literal that is not part of enumerate.
 *
 * @return The human readable string of enumerate literal value.
 */
extern char const * EnumToString(
	uint64_t     value,
	uint64_t     first,
	uint64_t     last,
	char const * valuestr,
	size_t       valuesz,
	char const * invalidstr);

/*----------------------------------------------------------------------------*/
/*! @brief Convert human readable string to enumerate literal.
 *
 * This operation converts human readable string to enumerate literal.
 * 
 * @param[in] str      The human readable string.
 * @param[in] first    The first enumerate literal value.
 * @param[in] last     The last enumerate literal value.
 * @param[in] valuestr The array of null terminated strings that represent the 
 *                     enumerate literals.
 * @param[in] valuesz  The size of a single element in the enumerate literal 
 *                     string array.
 * 
 * @return The flags.
 */
extern uint64_t EnumFromString(
	const std::string_view str,
	uint64_t               first,
	uint64_t               last,
	char const *           valuestr,
	size_t                 valuesz);

/*----------------------------------------------------------------------------*/
/*! @brief Convert enumerate literal to stream.
 *
 * This operation converts enumerate literal value into human readable stream. 
 * 
 * @param[out] os         The output stream.
 * @param[in]  value      The enumerate literal value to convert.
 * @param[in]  first      The first enumerate literal value.
 * @param[in]  last       The last enumerate literal value.
 * @param[in]  valuestr   The array of null terminated strings that represent 
 *                        the enumerate literals.
 * @param[in]  valuesz    The size of a single element in the enumerate literal 
 *                        string array.
 * @param[in]  invalidstr The null terminated string that represent the
 *                        enumerate literal that is not part of enumerate.
 *
 * @return The human readable string of enumerate literal value.
 */
extern std::ostream& EnumToStream(
	std::ostream & os,
	uint64_t       value,
	uint64_t       first,
	uint64_t       last,
	char const *   valuestr,
	size_t         valuesz,
	char const *   invalidstr);

/*----------------------------------------------------------------------------*/
/*! @brief Convert human readable stream to enumerate literal.
 *
 * This operation converts human readable stream to enumerate literal.
 * 
 * @param[in]  is       The input stream.
 * @param[out] value    The enumerate literal value.
 * @param[in]  first    The first enumerate literal value.
 * @param[in]  last     The last enumerate literal value.
 * @param[in]  valuestr The array of null terminated strings that represent the 
 *                      enumerate literals.
 * @param[in]  valuesz  The size of a single element in the enumerate literal 
 *                      string array.
 * 
 * @return The input stream.
 */
extern std::istream& EnumFromStream(
	std::istream & is,
	uint64_t &     value,
	uint64_t       first,
	uint64_t       last,
	char const *   valuestr,
	size_t         valuesz);

} /* namespace cxxwin */

#endif /* H11C9F1DB_7E5F_482B_B48E_FA6B60C677D6 */
