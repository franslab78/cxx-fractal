/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The mouse buttons enumerate.
 *
 * This file contains a class that represents mouse buttons enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef H7600E0A5_F19E_4277_A694_CC581A7D16B4
#define H7600E0A5_F19E_4277_A694_CC581A7D16B4

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief The mouse buttons.
 *
 * The mouse buttons.
 */
class MouseButtons
{
public:
	enum value_t
	{
		/*! @brief Left
		 *
		 * The left mouse button.
		 */
		Left,
		/*! @brief Right
		 *
		 * The right mouse button.
		 */
		Right,
		/*! @brief Middle
		 *
		 * The middle mouse button.
		 */
		Middle,
		/*! @brief X1
		 *
		 * The X1 mouse button.
		 */
		X1,
		/*! @brief X2
		 *
		 * The X2 mouse button.
		 */
		X2,
		Last
	};

	MouseButtons() = default;
	constexpr MouseButtons(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(MouseButtons a) const { return value_ == a.value_; }
	constexpr bool operator!=(MouseButtons a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, MouseButtons a) { return value == a.value_; }
	friend bool operator!=(value_t value, MouseButtons a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal.
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(MouseButtons value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string.
	 *
	 * @return The enumerate literal.
	 */
	static MouseButtons FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, MouseButtons value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, MouseButtons & value);

private:
	value_t value_ = Last;
};

} /* namespace cxxwin */

#endif /* H7600E0A5_F19E_4277_A694_CC581A7D16B4 */
