/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The main windows application
 *
 * This file contains a class that create the main windows application.
 *
 * @author Frans Labuschagne
 * @date   2020-01-13
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H09DBD2E8_348E_4512_BBF4_89C40A4CFDA6
#define H09DBD2E8_348E_4512_BBF4_89C40A4CFDA6

#include "cxxwin/windows_includes.hpp"
#include "cxxwin/window_class_styles.hpp"
#include "cxxwin/window_styles.hpp"
#include "cxxwin/mouse_virtual_keys.hpp"
#include "cxxwin/key_states.hpp"
#include "cxxwin/hot_keys.hpp"
#include "cxxwin/show_commands.hpp"
#include "cxxwin/resize_types.hpp"
#include "cxxwin/activate_types.hpp"
#include "cxxwin/button_directions.hpp"
#include "cxxwin/scroll_directions.hpp"
#include "cxxwin/mouse_buttons.hpp"
#include "cxxwin/key_directions.hpp"
#include "cxxwin/show_window_statuses.hpp"

#include <cstdint>
#include <string>
#include <string_view>

#include "nano_signal_slot.hpp"

namespace cxxwin
{

/*! @brief Application with main window.
 *
 * This class contains the
 *   - application message loop and
 *   - main window of the application
 */
class Application
{
public:
	/*! @brief Default constructor.
	 *
	 * This constructor shall initialise the Application to an invalid
	 * Application value. The class derived from the Application class needs to
	 * create the Application.
	 */
	Application() = default;

	/*! @brief Default destructor.
	 *
	 * This destructor shall close the Application if it is valid.
	 */
	virtual
	~Application() noexcept;

	/*! @brief Copy construction.
	 *
	 * This constructor shall initialise the member variables with a copy of the
	 * source member variables.
	 *
	 * @param[in] source The source to copy the from.
	 */
	Application(Application const & source) = delete;

	/*! @brief Copy assignment.
	 *
	 * This assign operator shall assign the member variables from the other
	 * class to this class.
	 *
	 * @param[in] other The other class to assign to this class.
	 *
	 * @return This class with the other class member variables assigned to it.
	 */
	Application&
	operator=(Application const & other) = delete;

	/*! @brief Move construction.
	 *
	 * This constructor shall initialise the member variables by moving the
	 * source member variable to them.
	 *
	 * @param[in,out] source The source to move from.
	 */
	Application(Application && source) = default;

	/*! @brief Move assignment.
	 *
	 * This move assign operator shall move the member variables from the other
	 * class to this class.
	 *
	 * @param[in,out] other The other class to move the member variables from.
	 *
	 * @return This class with the other class member variables moved to it.
	 */
	Application&
	operator=(Application && other) = default;

	/*! @brief Create the main application.
	 *
	 * This constructor shall initialise the Application registering the windows
	 * class and creating the main window.
	 *
	 * @param[in] name             The windows class name.
	 * @param[in] caption          The window caption.
	 * @param[in] x                The x location of the window.
	 * @param[in] y                The y location of the window.
	 * @param[in] width            The width of the window.
	 * @param[in] height           The height of the window.
	 * @param[in] windowStyle      The style of the window.
	 * @param[in] windowClassStyle The window class style.
	 */
	Application(
		const std::string_view      name,
		const std::string_view      caption,
		int                         x                = CW_USEDEFAULT,
		int                         y                = CW_USEDEFAULT,
		int                         width            = CW_USEDEFAULT,
		int                         height           = CW_USEDEFAULT,
		WindowStyleFlags::type      windowStyle      =
			WindowStyleFlags::OverlappedWindow,
		WindowClassStyleFlags::type windowClassStyle =
			WindowClassStyleFlags::VerticalRedraw |
			WindowClassStyleFlags::HorizontalRedraw);

	/*! @brief Execute the application
	 *
	 * This operation shall execute the application message loop.
	 *
	 * @param[in] show How the show the window.
	 *
	 * @return The result when the application was closed.
	 */
	int Execute(ShowCommands show = ShowCommands());

	/*! @brief Get the Direct2D render target.
	 *
	 * This operation gets the Direct 2D render target.
	 *
	 * @return The Direct 2D render target.
	 */
	ID2D1HwndRenderTarget * RenderTarget() { return renderTarget_; };

	/*! @brief Get the Direct2D render target.
	 *
	 * This operation gets the Direct 2D render target.
	 *
	 * @return The Direct 2D render target.
	 */
	ID2D1HwndRenderTarget const * RenderTarget() const { return renderTarget_; };

public: /* Events */

	/*! @brief Null event.
	 *
	 * An application sends the null event if it wants to post a message that the
	 * recipient window will ignore.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() : Application("myapplication", "My Application")
	 *       {
	 *          OnNull.connect<&MyApplication::HandleNull>(this);
	 *       }
	 *    protected:
	 *       void HandleNull(WPARAM wparam, LPARAM lparam)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 */
	Nano::Signal<void(WPARAM,LPARAM)> OnNull;

	/*! @brief Client create event
	 *
	 * This event requests that a window be created when the application called
	 * CreateWindowEx or CreateWindow function. The event is sent before the
	 * function returns. The window procedure of the new window receives this
	 * message after the window is created, but before the window becomes
	 * visible.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnCreate.connect<&MyApplication::HandleCreate>(this);
	 *       }
	 *    protected:
	 *       bool HandleCreate(const CREATESTRUCT * const info)
	 *       {
	 *          ...
	 *          if(error)
	 *          {
	 *             return false; // Not OK to create window
	 *          }
	 *          ...
	 *          return true; // OK to create window
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type                       | Name | Description                                                                                     |
	 * | ---------------------------| ---- | ----------------------------------------------------------------------------------------------- |
	 * | const CREATESTRUCT * const | info | A pointer to a CREATESTRUCT structure that contains information about the window being created. |
	 *
	 * @par Returns
	 * | Type | Name   | Description                                                                                         |
	 * | ---- | ------ | --------------------------------------------------------------------------------------------------- |
	 * | bool | result | true to continue creation of the window                                                             |
	 * | ^    | ^      | false the window is destroyed and the CreateWindowEx or CreateWindow function returns a NULL handle |
	 */
	Nano::Signal<bool(const CREATESTRUCT * const)> OnCreate;

	/*! @brief Destroy event
	 *
	 * Sent when a window is being destroyed after the window is removed from the
	 * screen.
	 *
	 * This message is sent first to the window being destroyed and then to the
	 * child windows (if any) as they are destroyed. During the processing of the
	 * message, it can be assumed that all child windows still exist.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnDestroy.connect<&MyApplication::HandleDestroy>(this);
	 *       }
	 *    protected:
	 *       bool HandleDestroy()
	 *       {
	 *          PostQuitMessage(0);
	 *          return true;
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool()> OnDestroy;

	/*! @brief Move event
	 *
	 * Sent after a window has been moved.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnMove.connect<&MyApplication::HandleMove>(this);
	 *       }
	 *    protected:
	 *       bool HandleMove(int x, int y)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type | Name | Description                                                                 |
	 * | ---- | ---- | --------------------------------------------------------------------------- |
	 * | int  | x    | The x coordinate of the upper-left corner of the client area of the window. |
	 * | int  | y    | The y coordinate of the upper-left corner of the client area of the window. |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(int,int)> OnMove;

	/*! @brief Resize event
	 *
	 * Sent to a window after its size has changed.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnSize.connect<&MyApplication::HandleSize>(this);
	 *       }
	 *    protected:
	 *       bool HandleSize(ResizeTypes how, int width, int height)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type        | Name   | Description                     |
	 * | ----------- | ------ | ------------------------------- |
	 * | ResizeTypes | how    | The type of resizing requested. |
	 * | int         | width  | The width of the client area.   |
	 * | int         | height | The height of the client area.  |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(ResizeTypes, int, int)> OnSize;

	/*! @brief Activate event
	 *
	 * Sent to both the window being activated and the window being deactivated.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnActivate.connect<&MyApplication::HandleActivate>(this);
	 *       }
	 *    protected:
	 *       bool HandleActivate(ActivateTypes how, bool isMinimized, HWND hwnd)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type          | Name        | Description                                                          |
	 * | ------------- | ----------- | -------------------------------------------------------------------- |
	 * | ActivateTypes | how         | How is the window being activated.                                   |
	 * | bool          | isMinimized | Is the window minimised.                                             |
	 * | HWND          | hwnd        | A handle to the window being activated or deactivated (can be null): |
	 * | ^             | ^           | how = InActive then handle to the other window being activated       |
	 * | ^             | ^           | how != InActive then handle to the other window being deactivated    |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(ActivateTypes, bool, HWND)> OnActivate;

	/*! @brief Focus event
	 *
	 * Sent to a window after it has gained/lost the keyboard focus.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnFocus.connect<&MyApplication::HandleFocus>(this);
	 *       }
	 *    protected:
	 *       bool HandleFocus(bool hasFocus, HWND hwnd)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type          | Name     | Description                                                          |
	 * | ------------- | -------- | -------------------------------------------------------------------- |
	 * | bool          | hasFocus | Has the window gained or lost focus.                                 |
	 * | HWND          | hwnd     | A handle to the other window (can be null):                          |
	 * | ^             | ^        | hasFocus = true then handle to the other window that lost focus      |
	 * | ^             | ^        | hasFocus = false then handle to the other window that gained focus   |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(bool,HWND)> OnFocus;

	/*! @brief Enable state change event
	 *
	 * Sent when an application changes the enabled state of a window. It is sent
	 * to the window whose enabled state is changing. This message is sent before
	 * the EnableWindow function returns, but after the enabled state
	 * (WS_DISABLED style bit) of the window has changed.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnEnable.connect<&MyApplication::HandleEnable>(this);
	 *       }
	 *    protected:
	 *       bool HandleEnable(bool isEnabled);
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type | Name      | Description                                                                                                                                                |
	 * | ---- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
	 * | bool | isEnabled | Indicates whether the window has been enabled or disabled. This parameter is true if the window has been enabled or false if the window has been disabled. |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(bool)> OnEnable;

	/*! @brief Client paint event
	 *
	 * The WM_PAINT message is sent when the system or another application makes
	 * a request to paint a portion of an application's window. The message is
	 * sent when the UpdateWindow or RedrawWindow function is called.
	 *
	 * An application may call the GetUpdateRect function to determine whether
	 * the window has an update region. If GetUpdateRect returns zero, the
	 * application need not call the BeginPaint and EndPaint functions.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnPaint.connect<&MyApplication::HandlePaint>(this);
	 *       }
	 *    protected:
	 *       bool HandlePaint()
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool()> OnPaint;

	/*! @brief Erase background event
	 *
	 * Sent when the window background must be erased (for example, when a window
	 * is resized). The message is sent to prepare an invalidated portion of a
	 * window for painting.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnEraseBackground.connect<&MyApplication::HandleEraseBackground>(this);
	 *       }
	 *    protected:
	 *       bool HandleEraseBackground(HDC hdc)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type | Name | Description                     |
	 * | ---- | ---- | ------------------------------- |
	 * | HDC  | hdc  | A handle to the device context. |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | -------------------------------------------------------------------------- |
	 * | bool | handled | true if the event was handled (the background was erased), otherwise false |
	 */
	Nano::Signal<bool(HDC)> OnEraseBackground;

	/*! @brief Set cursor event
	 *
	 * Sent to a window if the mouse causes the cursor to move within a window
	 * and mouse input is not captured.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnSetCursor.connect<&MyApplication::HandleSetCursor>(this);
	 *       }
	 *    protected:
	 *       bool HandleSetCursor(HWND hwnd, unsigned msg, int x, int y)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type     | Name | Description                                                                                                                      |
	 * | -------- | ---- | -------------------------------------------------------------------------------------------------------------------------------- |
	 * | HWND     | hwnd | A handle to the window that contains the cursor.                                                                                 |
	 * | unsigned | msg  | the mouse window message which triggered this event, such as WM_MOUSEMOVE. When the window enters menu mode, this value is zero. |
	 * | int      | x    | x-coordinate of the cursor. The coordinate is relative to the upper-left corner of the screen.                                   |
	 * | int      | y    | y-coordinate of the cursor. The coordinate is relative to the upper-left corner of the screen.                                   |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(HWND, unsigned, int, int)> OnSetCursor;

	/*! @brief Show window event
	 *
	 * Sent to a window when the window is about to be hidden or shown.
	 *
	 * The DefWindowProc function hides or shows the window, as specified by the
	 * message. If a window has the WS_VISIBLE style when it is created, the
	 * window receives this message after it is created, but before it is
	 * displayed. A window also receives this message when its visibility state
	 * is changed by the ShowWindow or ShowOwnedPopups function.
    *
    * The WM_SHOWWINDOW message is not sent under the following circumstances:
    *    - When a top-level, overlapped window is created with the WS_MAXIMIZE
    *      or WS_MINIMIZE style.
    *    - When the SW_SHOWNORMAL flag is specified in the call to the
    *      ShowWindow function.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnShowWindow.connect<&MyApplication::HandleShowWindow>(this);
	 *       }
	 *    protected:
	 *       bool HandleShowWindow(bool isShown, unsigned status)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type               | Name | Description                                            |
	 * | ------------------ | ------- | --------------------------------------------------- |
	 * | bool               | isShown | Is the window being shown (true) or hidden (false). |
	 * | ShowWindowStatuses | status  | The status of the window being shown.               |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(bool, ShowWindowStatuses)> OnShowWindow;

	/*! @brief Close event
	 *
	 * Sent as a signal that a window or an application should terminate.
	 *
	 * An application can prompt the user for confirmation, prior to destroying a
	 * window, by processing the close event and calling the DestroyWindow
	 * function only if the user confirms the choice.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnClose.connect<&MyApplication::HandleClose>(this);
	 *       }
	 *    protected:
	 *       bool HandleClose()
	 *       {
	 *          if(canClose())
	 *          {
	 *             DestroyWindow(windowHandle_);
	 *          }
	 *          return true;
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool()> OnClose;

	/*! @brief Display change event
	 *
	 * Sent to all top-level windows when the display resolution has changed.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnDisplayChange.connect<&MyApplication::HandleDisplayChange>(this);
	 *       }
	 *    protected:
	 *       bool HandleDisplayChange(int depth, int width, int height)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type | Name   | Description                                            |
	 * | ---- | ------ | ------------------------------------------------------ |
	 * | int  | depth  | The new image depth of the display, in bits per pixel. |
	 * | int  | width  | The horizontal resolution of the screen.               |
	 * | int  | height | The vertical resolution of the screen.                 |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(int, int, int)> OnDisplayChange;

	/*! @brief Non-client create event.
	 *
	 * Sent when an application requests that a window be created by calling the
	 * CreateWindowEx or CreateWindow function. The event is sent before
	 * WM_CREATE. The message is sent before the function returns.
	 *
	 * If an application processes this message, it should return true to
	 * continue creation of the window. If the application returns false, the
	 * CreateWindow or CreateWindowEx function will return a NULL handle.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnNcCreate.connect<&MyApplication::HandleNcCreate>(this);
	 *       }
	 *    protected:
	 *       bool HandleCreate(const CREATESTRUCT * const info)
	 *       {
	 *          ...
	 *          if(error)
	 *          {
	 *             return false; // Not OK to create window
	 *          }
	 *          ...
	 *          return true; // OK to create window
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type                       | Name | Description                                                                                     |
	 * | ---------------------------| ---- | ----------------------------------------------------------------------------------------------- |
	 * | const CREATESTRUCT * const | info | A pointer to a CREATESTRUCT structure that contains information about the window being created. |
	 *
	 * @par Returns
	 * | Type | Name   | Description                                                                                         |
	 * | ---- | ------ | --------------------------------------------------------------------------------------------------- |
	 * | bool | result | true to continue creation of the window                                                             |
	 * | ^    | ^      | false the window is destroyed and the CreateWindowEx or CreateWindow function returns a NULL handle |
	 */
	Nano::Signal<bool(const CREATESTRUCT * const)> OnNcCreate;

	/*! @brief Non-client destroy event
	 *
	 * Notifies a window that its non--client area is being destroyed.
	 * The DestroyWindow function sends the WM_NCDESTROY message to the window
	 * following the WM_DESTROY message. WM_DESTROY is used to free the
	 * allocated memory object associated with the window.
    *
    * The WM_NCDESTROY message is sent after the child windows have been
    * destroyed. In contrast, WM_DESTROY is sent before the child windows are
    * destroyed.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnNcDestroy.connect<&MyApplication::HandleNcDestroy>(this);
	 *       }
	 *    protected:
	 *       bool HandleNcDestroy()
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool()> OnNcDestroy;

	/*! @brief Non-client paint event
	 *
	 * The WM_NCPAINT message is sent to a window when its frame must be painted.
	 *
	 * An application can intercept the WM_NCPAINT message and paint its own
	 * custom window frame. The clipping region for a window is always
	 * rectangular, even if the shape of the frame is altered.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnNcPaint.connect<&MyApplication::HandleNcPaint>(this);
	 *       }
	 *    protected:
	 *       bool HandleNcPaint(HDC hdc, HRGN updateRegion)
	 *       {
	 *          ...
	 *       }
	 * };
	 *
	 * void MyApplication::HandleNcPaint(HDC hdc, HRGN updateRegion)
	 * {
	 *    ...
	 * }
	 * @endcode
	 *
	 * @par Parameters
	 * | Type | Name         | Description                     |
	 * | ---- | ------------ | ------------------------------- |
	 * | HDC  | hdc          |  |
	 * | HRGN | updateRegion |  |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(HDC, HRGN)> OnNcPaint;

	/*! @brief Key event
	 *
	 * Sent to the focus window when the mouse wheel is rotated. The
	 * DefWindowProc function propagates the message to the window's parent.
	 * There should be no internal forwarding of the message, since
	 * DefWindowProc propagates it up the parent chain until it finds a window
	 * that processes it.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnKey.connect<&MyApplication::HandleKey>(this);
	 *       }
	 *    protected:
	 *       bool HandleKey(KeyDirections       direction,
	 *                      unsigned            virtualCode,
	 *                      unsigned            scanCode,
	 *                      unsigned            repeatCount,
	 *                      KeyStateFlags::type flags)
	 *       {
	 *          ...
	 *       }
	 * @endcode
	 *
	 * @par Parameters
	 * | Type                | Name            | Description                     |
	 * | ------------------- | --------------- | ------------------------------- |
	 * | KeyDirections       | direction       | The direction of the key. |
	 * | unsigned            | virtualCode     | The virtual-key code of the non-system key.  |
	 * | unsigned            | scanCode        | The scan code. The value depends on the OEM. |
	 * | unsigned            | repeatCount     | The repeat count for the current message.    |
	 * | KeyStateFlags::type | flags           | bit 24: Indicates whether the key is an extended key, such as the right-hand ALT and CTRL keys that appear on an enhanced 101- or 102-key keyboard.
	 * | ^                   | ^               | bit 29: The context code: |
	 * | ^                   | ^               | direction Down, Up: always 0 |
	 * | ^                   | ^               | direction SystemDown, SystemUp: The value is 1 if the ALT key is down while the key is released; it is zero if the WM_SYSKEYDOWN message is posted to the active window because no window has the keyboard focus. |
	 * | ^                   | ^               | direction Character, SystemCharacter: The value is 1 if the ALT key is held down while the key is pressed; otherwise, the value is 0. |
	 * | ^                   | ^               | bit 30: The previous key state:
	 * | ^                   | ^               | direction Down, SystemDown: The value is 1 if the key is down before the message is sent, or it is zero if the key is up. |
	 * | ^                   | ^               | direction Up, SystemUp: always 1. |
	 * | ^                   | ^               | direction Character, SystemCharacter: The value is 1 if the key is down before the message is sent, or it is 0 if the key is up. |
	 * | ^                   | ^               | bit 31: The transition state:
	 * | ^                   | ^               | direction Down, SystemDown: always 0. |
	 * | ^                   | ^               | direction Up, SystemUp: always 1. |
	 * | ^                   | ^               | direction Character, SystemCharacter: The value is 1 if the key is being released, or it is 0 if the key is being pressed. |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(KeyDirections, unsigned, unsigned, unsigned,	KeyStateFlags::type)> OnKey;

	/*! @brief Mouse move event
	 *
	 * Sent to the focus window when the mouse wheel is rotated. The
	 * DefWindowProc function propagates the message to the window's parent.
	 * There should be no internal forwarding of the message, since
	 * DefWindowProc propagates it up the parent chain until it finds a window
	 * that processes it.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() : Application("myapplication", "My Application")
	 *       {
	 *          OnMouseMove.connect<&MyApplication::HandleMouseMove>(this);
	 *       }
	 *    protected:
	 *       bool HandleMouseMove(int x, int y, MouseVirtualKeyFlags::type virtualKeys)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type                 | Name        | Description                                                                                             |
	 * | -------------------- | ----------- | ------------------------------------------------------------------------------------------------------- |
	 * | int                  | x           | The x-coordinate of the cursor. The coordinate is relative to the upper-left corner of the client area. |
	 * | int                  | y           | The y-coordinate of the cursor. The coordinate is relative to the upper-left corner of the client area. |
	 * | MouseVirtualKeyFlags | virtualKeys | Indicates whether various virtual keys are down.                                                        |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(int, int, MouseVirtualKeyFlags::type)> OnMouseMove;

	/*! @brief Mouse move event
	 *
	 * Sent to the focus window when the mouse wheel is rotated. The
	 * DefWindowProc function propagates the message to the window's parent.
	 * There should be no internal forwarding of the message, since
	 * DefWindowProc propagates it up the parent chain until it finds a window
	 * that processes it.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() : Application("myapplication", "My Application")
	 *       {
	 *          OnMouseButton.connect<&MyApplication::HandleMouseButton>(this);
	 *       }
	 *    protected:
	 *       bool HandleMouseButton(MouseButtons button, ButtonDirection direction, int x, int y, MouseVirtualKeyFlags::type virtualKeys)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type                 | Name        | Description                                                                                             |
	 * | -------------------- | ----------- | ------------------------------------------------------------------------------------------------------- |
	 * | MouseButtons         | button      | The mouse button that caused the event.                                                                 |
	 * | ButtonDirection      | direction   | The direction of the mouse button.                                                                      |
	 * | int                  | x           | The x-coordinate of the cursor. The coordinate is relative to the upper-left corner of the client area. |
	 * | int                  | y           | The y-coordinate of the cursor. The coordinate is relative to the upper-left corner of the client area. |
	 * | MouseVirtualKeyFlags | virtualKeys | Indicates whether various virtual keys are down.                                                        |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(MouseButtons, ButtonDirections, int, int, MouseVirtualKeyFlags::type)> OnMouseButton;

	/*! @brief Mouse wheel event
	 *
	 * Sent to the focus window when the mouse wheel is rotated. The
	 * DefWindowProc function propagates the message to the window's parent.
	 * There should be no internal forwarding of the message, since
	 * DefWindowProc propagates it up the parent chain until it finds a window
	 * that processes it.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnMouseWheel.connect<&MyApplication::HandleMouseWheel>(this);
	 *       }
	 *    protected:
	 *       bool HandleMouseWheel(ScrollDirection direction, int delta, int x, int y, unsigned fncKeys)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type            | Name      | Description                                                                       |
	 * | --------------- | --------- | --------------------------------------------------------------------------------- |
	 * | ScrollDirection | direction | The scroll direction.                                                             |
	 * | int             | delta     | The scroll amount.                                                                |
	 * | int             | x         | The x-coordinate of the pointer, relative to the upper-left corner of the screen. |
	 * | int             | y         | The y-coordinate of the pointer, relative to the upper-left corner of the screen. |
	 * | unsigned        | fncKeys   | Indicates whether various virtual keys are down.                                  |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(ScrollDirections, int, int, int, unsigned)> OnMouseWheel;

	/*! @brief Mouse hover event
	 *
	 * Posted to a window when the cursor hovers over the client area of the
	 * window for the period of time specified in a prior call to TrackMouseEvent.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnMouseHover.connect<&MyApplication::HandleMouseHover>(this);
	 *       }
	 *    protected:
	 *       bool HandleMouseHover(int x, int y, unsigned fncKeys)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type     | Name      | Description                                                                       |
	 * | ---------| --------- | --------------------------------------------------------------------------------- |
	 * | int      | x         | The x-coordinate of the pointer, relative to the upper-left corner of the screen. |
	 * | int      | y         | The y-coordinate of the pointer, relative to the upper-left corner of the screen. |
	 * | unsigned | fncKeys   | Indicates whether various virtual keys are down.                                  |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(int, int, unsigned)> OnMouseHover;

	/*! @brief Mouse leave event
	 *
	 * Posted to a window when the cursor leaves the client area of the window
	 * specified in a prior call to TrackMouseEvent.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnMouseLeave.connect<&MyApplication::HandleMouseLeave>(this);
	 *       }
	 *    protected:
	 *       bool HandleMouseLeave()
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool()> OnMouseLeave;

	/*! @brief Hot key event
	 *
	 * Posted when the user presses a hot key registered by the RegisterHotKey
	 * function. The message is placed at the top of the message queue associated
	 * with the thread that registered the hot key.
	 *
	 * @par Usage
	 * @code{.cpp}
	 * class MyApplication final : public cxxwin::Application
	 * {
	 *    public:
	 *       MyApplication() :
	 *          Application("myapplication", "My Application")
	 *       {
	 *          OnHotKey.connect<&MyApplication::HandleHotKey>(this);
	 *       }
	 *    protected:
	 *       bool HandleHotKey(int, unsigned, HotKeyFlags::type)
	 *       {
	 *          ...
	 *       }
	 * };
	 * @endcode
	 *
	 * @par Parameters
	 * | Type              | Name            | Description                                                                                                                                                             |
	 * | ----------------- | --------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
	 * | int               | id              | The identifier of the hot key that generated the message. If the message was generated by a system-defined hot key, this parameter will be one of the following values. |
	 * | ^                 | ^               | IDHOT_SNAPDESKTOP(-2): The "snap desktop" hot key was pressed.                                                                                                          |
	 * | ^                 | ^               | IDHOT_SNAPWINDOW(-1): The "snap window" hot key was pressed.                                                                                                            |
	 * | unsigned          | virtualCode     | The virtual-key code of the key.                                                                                                                                        |
	 * | HotKeyFlags::type | combinationWith | The keys that were pressed in combination with the hot-key.                                                                                                             |
	 *
	 * @par Returns
	 * | Type | Name    | Description                                    |
	 * | ---- | ------  | ---------------------------------------------- |
	 * | bool | handled | true if the event was handled, otherwise false |
	 */
	Nano::Signal<bool(int, unsigned, HotKeyFlags::type)> OnHotKey;

protected:
	/*! @brief Create the device resources.
	 *
	 * This operation creates the Direct2D render target for the application main
	 * window.
	 */
	virtual HRESULT CreateDeviceResources();

	/*! @brief Discard the device resources.
	 *
	 * This operation discards the Direct2D render target for the application
	 * main window.
	 */
	virtual void DiscardDeviceResources();

	/*! @brief The window message handler.
	 *
	 * The application main window message handler will translate windows
	 * messages and trigger application events.
	 *
	 * WM_USER through (WM_APP-1): Integer messages for use by private window
	 * classes.
	 */
	[[nodiscard]] virtual LRESULT
	SystemMessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam);

	/*! @brief The window message handler.
	 *
	 * The application main window message handler will translate windows
	 * messages and trigger application events.
	 *
	 * WM_USER through (WM_APP-1): Integer messages for use by private window
	 * classes.
	 */
	[[nodiscard]] virtual LRESULT
	UserMessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam);

	/*! @brief The window message handler.
	 *
	 * The application main window message handler will translate windows
	 * messages and trigger application events.
	 *
	 * WM_APP through 0xBFFF: Messages available for use by applications.
	 */
	[[nodiscard]] virtual LRESULT
	ApplicationMessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam);

	/*! @brief The handle to the main application window. */
	HWND                    windowHandle_    = nullptr;
	/*! @brief The Direct2D factory. */
	ID2D1Factory *          direct2dFactory_ = nullptr;
	/*! @brief The DirectWriteFactory */
	IDWriteFactory *        directWriteFactory_ = nullptr;
	/*! @brief The Direct2D render target. */
	ID2D1HwndRenderTarget * renderTarget_    = nullptr;

	/*! @brief Get error code string.
	 *
	 * This operation gets the error code string.
	 *
	 * \param[in] code The error code.
	 *
	 * \return The error code string.
	 */
	static std::string DescriptionLastError(DWORD code);

	/*! @brief Get error code string.
	 *
	 * This operation gets the error code string.
	 *
	 * \param[in] code The error code.
	 *
	 * \return The error code string.
	 */
	static std::string DescriptionHResult(HRESULT code);

	static std::wstring toWideString(std::string_view const string);

	static std::string toString(std::wstring_view const string);

private:
	/*! @brief Windows Procedure
	 *
	 * The windows procedure for the application main window. This window
	 * procedure will call the application MessageHandler member function.
	 *
	 * https://docs.microsoft.com/en-us/windows/win32/winmsg/window-procedures
	 */
	static LRESULT __stdcall WndProc(
		HWND const window,
		UINT const message,
		WPARAM const wparam,
		LPARAM const lparam);

	/*! @brief The window message handler.
	 *
	 * The application main window message handler will translate windows
	 * messages and trigger application events.
	 */
	[[nodiscard]] LRESULT
	MessageHandler(
		UINT const   message,
		WPARAM const wparam,
		LPARAM const lparam);

	/*! @brief The main window class atom.
	 *
	 * The atom to the registered window class for the application main window.
	 */
	uint16_t atom_ = 0;
};

} /* namespace cxxwin */

#endif /* H09DBD2E8_348E_4512_BBF4_89C40A4CFDA6 */
