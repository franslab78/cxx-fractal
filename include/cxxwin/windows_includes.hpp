/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The common window includes.
 *
 * This file contains the common windows includes.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H6C28B41D_EFA8_4154_A9F1_A23354D7ECFB
#define H6C28B41D_EFA8_4154_A9F1_A23354D7ECFB

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN    // Exclude rarely-used stuff from Windows headers
#endif

#ifndef WIN32_DESIRED
#define WIN32_DESIRED 0x0601 /* WIN7 */
//#define WIN32_DESIRED 0x0602 /* WIN8 */
//#define WIN32_DESIRED 0x0A00 /* WIN10 */
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT WIN32_DESIRED
#else
#if _WIN32_WINNT < WIN32_DESIRED
#undef _WIN32_WINNT
#define _WIN32_WINNT WIN32_DESIRED
#endif
#endif

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif

#include <SDKDDKVer.h>

#include <Windows.h>
#include <windowsx.h>

#include <d2d1.h>
#include <dwrite.h>

#endif /* H6C28B41D_EFA8_4154_A9F1_A23354D7ECFB */
