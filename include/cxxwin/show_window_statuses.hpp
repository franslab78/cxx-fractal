/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The show windows statuses enumerate.
 *
 * This file contains a class that represents show windows statuses enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef HE0651DE6_03D0_4C47_ABFA_674090806A43
#define HE0651DE6_03D0_4C47_ABFA_674090806A43

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Show window statuses.
 *
 * The Show window statuses.
 *
 * https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-showwindow
 */
class ShowWindowStatuses
{
public:
	enum value_t
	{
		/*! @brief
		 *
		 * The message was sent because of a call to the ShowWindow function.
		 */
		Called,

		/*! @brief
		 *
		 * The window's owner window is being minimized.
		 */
		Closing = SW_PARENTCLOSING,
		/*! @brief
		 *
		 * The window is being covered by another window that has been 
		 * maximized.
		 */
		OtherZoom = SW_OTHERZOOM,
		/*! @brief
		 *
		 * The window's owner window is being restored.
		 */
		ParentOpening = SW_PARENTOPENING,
		/*! @brief
		 *
		 * The window is being uncovered because a maximize window was restored 
		 * or minimized.
		 */
		OtherUnzoom = SW_OTHERUNZOOM
	};

	ShowWindowStatuses() = default;
	constexpr ShowWindowStatuses(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(ShowWindowStatuses a) const { return value_ == a.value_; }
	constexpr bool operator!=(ShowWindowStatuses a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, ShowWindowStatuses a) { return value == a.value_; }
	friend bool operator!=(value_t value, ShowWindowStatuses a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(ShowWindowStatuses value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string.
	 *
	 * @return The enumerate literal.
	 */
	static ShowWindowStatuses FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, ShowWindowStatuses value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, ShowWindowStatuses & value);

private:
	value_t value_ = Called;
};

} /* namespace cxxwin */

#endif /* HE0651DE6_03D0_4C47_ABFA_674090806A43 */
