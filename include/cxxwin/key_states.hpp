/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The key state flags.
 *
 * This file contains a class that represents key state flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-12
 */
/*----------------------------------------------------------------------------*/

#pragma once

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

#ifndef HD6A70EAD_150F_472C_AC65_69E5233701B3
#define HD6A70EAD_150F_472C_AC65_69E5233701B3

namespace cxxwin
{

/*! @brief The key state flags.
 *
 * Key state flags.
 *
 * https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-char
 * https://docs.microsoft.com/en-us/windows/win32/menurc/wm-syschar
 */
struct KeyStateFlags
{
	/*! @brief The type used for the flags. */
	using type = uint32_t;

	/*! @brief The left mouse button is down.
	 *
	 * The left mouse button is down.
	 */
	static constexpr type IsExtended = 0x01000000;
	/*! @brief The right mouse button is down.
	 *
	 * The right mouse button is down.
	 */
	static constexpr type ContextCode = 0x20000000;
	/*! @brief The SHIFT key is down.
	 *
	 * The SHIFT key is down.
	 */
	static constexpr type PreviousState = 0x40000000;
	/*! @brief The CTRL key is down.
	 *
	 * The CTRL key is down.
	 */
	static constexpr type TransitionState = 0x80000000;

	/*! @brief The CTRL key is down.
	 *
	 * The CTRL key is down.
	 */
	static constexpr type MaskAll = IsExtended | ContextCode | PreviousState | TransitionState;

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the flags to a human readable string.
	 *
	 * @param[in] flags The flags.
	 *
	 * @return The human readable flags string.
	 */
	static std::string ToString(type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in] str The flags string.
	 *
	 * @return The flags.
	 */
	static type FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the flags to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     flags The flags.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    flags The flags.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, type & flags);
};

} /* namespace cxxwin */

#endif /* HD6A70EAD_150F_472C_AC65_69E5233701B3 */
