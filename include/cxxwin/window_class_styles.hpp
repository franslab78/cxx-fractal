/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The window class style flags
 *
 * This file contains a class that represents window class style flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef HAADBE04F_8232_45BD_BBE5_B0168D0B2F7E
#define HAADBE04F_8232_45BD_BBE5_B0168D0B2F7E

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Windows class styles
 *
 * Windows class style flags.
 *
 * https://docs.microsoft.com/en-us/windows/win32/winmsg/window-class-styles
 */
struct WindowClassStyleFlags
{
	/*! @brief The type used for the flags. */
	using type = uint32_t;

	/*! @brief No style. */
	static constexpr type None = 0;

	/*! @brief Vertical redraw.
	 *
	 * Redraws the entire window if a movement or size adjustment changes the
	 * height of the client area.
	 */
	static constexpr type VerticalRedraw  = CS_VREDRAW;

	/*! @brief Horizontal redraw
	 *
	 * Redraws the entire window if a movement or size adjustment changes the
	 * width of the client area.
	 */
	static constexpr type HorizontalRedraw = CS_HREDRAW;

	/*! @brief Send double-click
	 *
	 * Sends a double-click message to the window procedure when the user
	 * double-clicks the mouse while the cursor is within a window belonging
	 * to the class.
	 */
	static constexpr type DoubleClicks = CS_DBLCLKS;

	/*! @brief Own device context
	 *
	 * Allocates a unique device context for each window in the class.
	 */
	static constexpr type OwnDeviceContext = CS_OWNDC;

	/*! @brief Shared device context
	 *
	 * Allocates one device context to be shared by all windows in the class.
	 * Because window classes are process specific, it is possible for
	 * multiple threads of an application to create a window of the same
	 * class. It is also possible for the threads to attempt to use the
	 * device context simultaneously. When this happens, the system allows
	 * only one thread to successfully finish its drawing operation.
	 */
	static constexpr type SharedDeviceContext = CS_CLASSDC;

	/*! @brief Parent device context
	 *
	 * Sets the clipping rectangle of the child window to that of the parent
	 * window so that the child can draw on the parent. A window with the
	 * CS_PARENTDC style bit receives a regular device context from the
	 * system's cache of device contexts. It does not give the child the
	 * parent's device context or device context settings. Specifying
	 * CS_PARENTDC enhances an application's performance.
	 */
	static constexpr type ParentDeviceContext = CS_PARENTDC;

	/*! @brief No close
	 *
	 * Disables Close on the window menu.
	 */
	static constexpr type NoClose = CS_NOCLOSE;

	/*! @brief Save as bitmap
	 *
	 * Saves, as a bitmap, the portion of the screen image obscured by a
	 * window of this class. When the window is removed, the system uses the
	 * saved bitmap to restore the screen image, including other windows that
	 * were obscured. Therefore, the system does not send WM_PAINT messages
	 * to windows that were obscured if the memory used by the bitmap has not
	 * been discarded and if other screen actions have not invalidated the
	 * stored image.
	 *
	 * This style is useful for small windows (for example, menus or dialog
	 * boxes) that are displayed briefly and then removed before other screen
	 * activity takes place. This style increases the time required to
	 * display the window, because the system must first allocate memory to
	 * store the bitmap.
	 */
	static constexpr type SaveAsBitmap = CS_SAVEBITS;

	/*! @brief Byte align client
	 *
	 * Aligns the window's client area on a byte boundary (in the x
	 * direction). This style affects the width of the window and its
	 * horizontal placement on the display.
	 */
	static constexpr type ByteAlignClient = CS_BYTEALIGNCLIENT;

	/*! @brief Byte align window
	 *
	 * Aligns the window on a byte boundary (in the x direction). This style
	 * affects the width of the window and its horizontal placement on the
	 * display.
	 */
	static constexpr type ByteAlignWindow = CS_BYTEALIGNWINDOW;

	/*! @brief Global class
	 *
	 * Indicates that the window class is an application global class.
	 * For more information, see the "Application Global Classes" section of
	 * About Window Classes.
	 *
	 * https://docs.microsoft.com/en-us/windows/win32/winmsg/about-window-classes
	 */
	static constexpr type Global = CS_GLOBALCLASS;

	/*! @brief Enable drop shadow.
	 *
	 * Enables the drop shadow effect on a window. The effect is turned on and
	 * off through SPI_SETDROPSHADOW. Typically, this is enabled for small,
	 * short-lived windows such as menus to emphasise their Z-order
	 * relationship to other windows. Windows created from a class with this
	 * style must be top-level windows; they may not be child windows.
	 */
	static constexpr type EnableDropShadow = CS_DROPSHADOW;

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the flags to a human readable string.
	 *
	 * @param[in] flags The flags.
	 *
	 * @return The human readable flags string.
	 */
	static std::string ToString(type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in] str The flags string.
	 *
	 * @return The flags.
	 */
	static type FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the flags to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     flags The flags.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    flags The flags.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, type & flags);
};

} /* namespace cxxwin */

#endif /* HAADBE04F_8232_45BD_BBE5_B0168D0B2F7E */
