/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The hot key flags.
 *
 * This file contains a class that represents hot key flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-12
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef H4F77F271_15F3_4D83_84CD_4C9F87079E57
#define H4F77F271_15F3_4D83_84CD_4C9F87079E57

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief The hot key flags.
 *
 * The hot key flags.
 *
 * https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-hotkey
 */
struct HotKeyFlags
{
	/*! @brief The type used for the flags. */
	using type = uint32_t;

	/*! @brief No mouse virtual key. */
	static constexpr type None = 0;

	/*! @brief Either ALT key was held down.
	 *
	 * Either ALT key was held down.
	 */
	static constexpr type Alt = MOD_ALT;
	/*! @brief Either CTRL key was held down.
	 *
	 * Either CTRL key was held down.
	 */
	static constexpr type Control = MOD_CONTROL;
	/*! @brief Either SHIFT key was held down.
	 *
	 * Either SHIFT key was held down.
	 */
	static constexpr type Shift = MOD_SHIFT;
	/*! @brief The WINDOWS key is down.
	 *
	 * Either WINDOWS key was held down. These keys are labelled with the
	 * Windows logo. Hotkeys that involve the Windows key are reserved for
	 * use by the operating system.
	 */
	static constexpr type Win = MOD_WIN;

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the flags to a human readable string.
	 *
	 * @param[in] flags The flags
	 *
	 * @return The human readable flags string.
	 */
	static std::string ToString(type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in] str The flags string
	 *
	 * @return The flags.
	 */
	static type FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the flags to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     flags The flags.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in,out] is    The output stream.
	 * @param[out]    flags The flags.
	 *
	 * @return The flags.
	 */
	static std::istream& FromStream(std::istream & is, type & flags);
};

} /* namespace cxxwin */

#endif /* H4F77F271_15F3_4D83_84CD_4C9F87079E57 */
