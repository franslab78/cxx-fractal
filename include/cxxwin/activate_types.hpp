/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The activate types enumerate.
 *
 * This file contains a class that represents activate types enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef HAD31F498_23F1_433F_A811_A68D89B891C0
#define HAD31F498_23F1_433F_A811_A68D89B891C0

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Type of window activation.
 *
 * The type of window activation.
 *
 * https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-activate
 */
class ActivateTypes
{
public:
	enum value_t
	{
		/*! @brief Inactive
		 *
		 * The windows is inactive.
		 */
		InActive = WA_INACTIVE,

		/*! @brief Active
		 *
		 * The windows is activated by some method other than a mouse click.
		 */
		Active = WA_ACTIVE,

		/*! @brief Click active
		 *
		 * The windows is activated by a mouse click.
		 */
		ClickActive = WA_CLICKACTIVE
	};

	ActivateTypes() = default;
	constexpr ActivateTypes(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(ActivateTypes a) const { return value_ == a.value_; }
	constexpr bool operator!=(ActivateTypes a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, ActivateTypes a) { return value == a.value_; }
	friend bool operator!=(value_t value, ActivateTypes a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal.
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(ActivateTypes value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string.
	 *
	 * @return The enumerate literal.
	 */
	static ActivateTypes FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, ActivateTypes value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, ActivateTypes & value);

private:
	value_t value_ = InActive;
};

} /* namespace cxxwin */

#endif /* HAD31F498_23F1_433F_A811_A68D89B891C0 */
