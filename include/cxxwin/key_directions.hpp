/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The key directions enumerate.
 *
 * This file contains a class that represents key directions enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef H49DD2E53_0323_42D4_99BC_56C77A311261
#define H49DD2E53_0323_42D4_99BC_56C77A311261

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Type of key directions.
 *
 * The type of key directions.
 */
class KeyDirections
{
public:
	enum value_t
	{
		/*! @brief Down
		 *
		 * The key is down.
		 */
		Down,
		/*! @brief Up
		 *
		 * The key is up.
		 */
		Up,
		/*! @brief System Down
		 *
		 * Posted to the window with the keyboard focus when the user presses the
		 * F10 key (which activates the menu bar) or holds down the ALT key and
		 * then presses another key.
		 */
		SystemDown,
		/*! @brief System Up
		 *
		 * Posted to the window with the keyboard focus when the user releases a
		 * key that was pressed while the ALT key was held down.
		 */
		SystemUp,
		/*! @brief Character
		 *
		 * The key character
		 */
		Character,
		/*! @brief System character
		 *
		 * Posted to the window with the keyboard focus when a WM_SYSKEYDOWN
		 * message is translated by the TranslateMessage function. It specifies
		 * the character code of a system character key that is, a character key
		 * that is pressed while the ALT key is down.
		 */
		SystemCharacter,

		Last
	};

	KeyDirections() = default;
	constexpr KeyDirections(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(KeyDirections a) const { return value_ == a.value_; }
	constexpr bool operator!=(KeyDirections a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, KeyDirections a) { return value == a.value_; }
	friend bool operator!=(value_t value, KeyDirections a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal.
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(KeyDirections value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string.
	 *
	 * @return The enumerate literal.
	 */
	static KeyDirections FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, KeyDirections value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input stream.
	 */
	static std::istream& FromStream(std::istream & is, KeyDirections & value);

private:
	value_t value_ = Last;
};

} /* namespace cxxwin */

#endif /* H49DD2E53_0323_42D4_99BC_56C77A311261 */
