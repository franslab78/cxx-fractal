/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The window style flags
 *
 * This file contains a class that represents window style flags.
 *
 * @author Frans Labuschagne
 * @date   2020-03-09
 */
/*----------------------------------------------------------------------------*/

#pragma once

#ifndef H84A6DE64_2533_4A24_A206_17E1E2347A61
#define H84A6DE64_2533_4A24_A206_17E1E2347A61

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief The window style flags.
 *
 * Windows style and extended style flags.
 *
 * https://docs.microsoft.com/en-us/windows/win32/winmsg/window-styles
 * https://docs.microsoft.com/en-us/windows/win32/winmsg/extended-window-styles
 */
struct WindowStyleFlags
{
	/*! @brief The type used for the flags. */
	using type = uint64_t;

	/*! @brief The window is an overlapped window.
	 *
	 * The window is an overlapped window. An overlapped window has a title
	 * bar and a border. Same as the Tiled style. */
	static constexpr type Overlapped = WS_OVERLAPPED;

	/*! @brief The window is an overlapped window.
	 *
	 * The window is an overlapped window. An overlapped window has a title
	 * bar and a border. Same as the Overlapped style. */
	static constexpr type Tiled = WS_TILED;

	/*! @brief The windows is a pop-up window.
	 *
	 * The windows is a pop-up window. This style cannot be used with the
	 * Child style.
	 */
	static constexpr type Popup = WS_POPUP;

	/*! @brief The window is a child window.
	 *
	 * The window is a child window. A window with this style cannot have a
	 * menu bar. This style cannot be used with the Popup style.
	 */
	static constexpr type Child = WS_CHILD;

	/*! @brief The window is initially minimised.
	 *
	 * The window is initially minimised. Same as the Iconic style.
	 */
	static constexpr type Minimise = WS_MINIMIZE;

	/*! @brief The window is initially minimised.
	 *
	 * The window is initially minimised. Same as the Minimise style.
	 */
	static constexpr type Iconic = WS_ICONIC;

	/*! @brief The window is initially visible.
	 *
	 * This style can be turned on and off by using the ShowWindow or
	 * SetWindowPos function.
	 */
	static constexpr type Visible = WS_VISIBLE;

	/*! @brief The window is initially disabled.
	 *
	 * The window is initially disabled. A disabled window cannot receive
	 * input from the user. To change this after a window has been created,
	 * use the EnableWindow function.
	 */
	static constexpr type Disabled = WS_DISABLED;

	/*! @brief Clips child windows relative to each other.
	 *
	 * Clips child windows relative to each other; that is, when a particular
	 * child window receives a WM_PAINT message, the WS_CLIPSIBLINGS style
	 * clips all other overlapping child windows out of the region of the
	 * child window to be updated. If WS_CLIPSIBLINGS is not specified and
	 * child windows overlap, it is possible, when drawing within the client
	 * area of a child window, to draw within the client area of a
	 * neighbouring child window.
	 */
	static constexpr type ClipSiblings = WS_CLIPSIBLINGS;

	/*! @brief Excludes the area occupied by child windows.
	 *
	 * Excludes the area occupied by child windows when drawing occurs within
	 * the parent window. This style is used when creating the parent window.
	 */
	static constexpr type ClipChildren = WS_CLIPCHILDREN;

	/*! @brief The window is initially maximised.
	 *
	 * The window is initially maximised.
	 */
	static constexpr type Maximised = WS_MAXIMIZE;

	/*! @brief The window has a title bar.
	 *
	 * The window has a title bar (includes the WS_BORDER style).
	 */
	static constexpr type Caption = WS_CAPTION;

	/*! @brief The window has a thin-line border.
	 *
	 * The window has a thin-line border.
	 */
	static constexpr type Border = WS_BORDER;

	/*! @brief The window has a border of a style typically used with dialog boxes.
	 *
	 * The window has a border of a style typically used with dialog boxes.
	 * A window with this style cannot have a title bar.
	 */
	static constexpr type DialogFrame = WS_DLGFRAME;

	/*! @brief The window has a vertical scroll bar.
	 *
	 * The window has a vertical scroll bar.
	 */
	static constexpr type VerticalScroll = WS_VSCROLL;

	/*! @brief The window has a horizontal scroll bar.
	 *
	 * The window has a horizontal scroll bar.
	 */
	static constexpr type HorizontalScroll = WS_HSCROLL;

	/*! @brief The window has a window menu on its title bar.
	 *
	 * The window has a window menu on its title bar. The Caption style
	 * must also be specified.
	 */
	static constexpr type SystemMenu = WS_SYSMENU;

	/*! @brief The window has a sizing border.
	 *
	 * The window has a sizing border. Same as the SizeBox style.
	 */
	static constexpr type ThickFrame = WS_THICKFRAME;

	/*! @brief The window has a sizing border.
	 *
	 * The window has a sizing border. Same as the ThickFrame style.
	 */
	static constexpr type SizeBox = WS_SIZEBOX;

	/*! @brief The window is the first control of a group of controls.
	 *
	 * The window is the first control of a group of controls. The group
	 * consists of this first control and all controls defined after it, up to
	 * the next control with the WS_GROUP style. The first control in each
	 * group usually has the WS_TABSTOP style so that the user can move from
	 * group to group. The user can subsequently change the keyboard focus
	 * from one control in the group to the next control in the group by
	 * using the direction keys.
	 *
	 * You can turn this style on and off to change dialog box navigation. To
	 * change this style after a window has been created, use the
	 * SetWindowLong function.
	 */
	static constexpr type Group = WS_GROUP;

	/*! @brief The window is a control that can receive the keyboard focus
	 * when the user presses the TAB key.
	 *
	 * The window is a control that can receive the keyboard focus when the
	 * user presses the TAB key. Pressing the TAB key changes the keyboard
	 * focus to the next control with the WS_TABSTOP style.
	 *
	 * You can turn this style on and off to change dialog box navigation. To
	 * change this style after a window has been created, use the
	 * SetWindowLong function. For user-created windows and modeless dialogs
	 * to work with tab stops, alter the message loop to call the
	 * IsDialogMessage function.
	 */
	static constexpr type TabStop = WS_TABSTOP;

	/*! @brief The window has a minimise button.
	 *
	 * The window has a minimise button. Cannot be combined with the
	 * WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
	 */
	static constexpr type MinimisedBox = WS_MINIMIZEBOX;

	/*! @brief The window has a maximise button.
	 *
	 * The window has a maximise button. Cannot be combined with the
	 * WS_EX_CONTEXTHELP style. The WS_SYSMENU style must also be specified.
	 */
	static constexpr type MaximisedBox        = WS_MAXIMIZEBOX;

	static constexpr type DialogModalFrame    = static_cast<type>(WS_EX_DLGMODALFRAME) << 32;
	static constexpr type NoParentNotify      = static_cast<type>(WS_EX_NOPARENTNOTIFY) << 32;
	static constexpr type TopMost             = static_cast<type>(WS_EX_TOPMOST) << 32;
	static constexpr type AcceptFiles         = static_cast<type>(WS_EX_ACCEPTFILES) << 32;
	static constexpr type Transparent         = static_cast<type>(WS_EX_TRANSPARENT) << 32;
	static constexpr type MdiChild            = static_cast<type>(WS_EX_MDICHILD) << 32;
	static constexpr type ToolWindow          = static_cast<type>(WS_EX_TOOLWINDOW) << 32;
	static constexpr type WindowEdge          = static_cast<type>(WS_EX_WINDOWEDGE) << 32;
	static constexpr type ClientEdge          = static_cast<type>(WS_EX_CLIENTEDGE) << 32;
	static constexpr type ContextHelp         = static_cast<type>(WS_EX_CONTEXTHELP) << 32;
	static constexpr type Right               = static_cast<type>(WS_EX_RIGHT) << 32;
	static constexpr type Left                = static_cast<type>(WS_EX_LEFT) << 32;
	static constexpr type RightToLeftReading  = static_cast<type>(WS_EX_RTLREADING) << 32;
	static constexpr type LeftToRightReading  = static_cast<type>(WS_EX_LTRREADING) << 32;
	static constexpr type LeftScrollBar       = static_cast<type>(WS_EX_LEFTSCROLLBAR) << 32;
	static constexpr type RightScrollBar      = static_cast<type>(WS_EX_RIGHTSCROLLBAR) << 32;
	static constexpr type ControlParent       = static_cast<type>(WS_EX_CONTROLPARENT) << 32;
	static constexpr type StaticEdge          = static_cast<type>(WS_EX_STATICEDGE) << 32;
	static constexpr type ApplicationWindow   = static_cast<type>(WS_EX_APPWINDOW) << 32;
	static constexpr type Layered             = static_cast<type>(WS_EX_LAYERED) << 32;
	static constexpr type NoInheritLayout     = static_cast<type>(WS_EX_NOINHERITLAYOUT) << 32;
#if _WIN32_WINNT >= _WIN32_WINNT_WIN8
	static constexpr type NoRedirectionBitmap = static_cast<type>(WS_EX_NOREDIRECTIONBITMAP) << 32;
#endif
	static constexpr type LayoutRightToLeft   = static_cast<type>(WS_EX_LAYOUTRTL) << 32;
	static constexpr type Composited          = static_cast<type>(WS_EX_COMPOSITED) << 32;
	static constexpr type NoActivate          = static_cast<type>(WS_EX_NOACTIVATE) << 32;

	static constexpr type OverlappedWindow    = WS_OVERLAPPEDWINDOW | (static_cast<type>(WS_EX_OVERLAPPEDWINDOW) << 32);
	static constexpr type PopupWindow         = WS_POPUPWINDOW;
	static constexpr type ChildWindow         = WS_CHILDWINDOW;
	static constexpr type PaletteWindow       = static_cast<type>(WS_EX_PALETTEWINDOW) << 32;

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the flags to a human readable string.
	 *
	 * @param[in] flags The flags
	 *
	 * @return The human readable flags string.
	 */
	static std::string ToString(type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in] str The flags string
	 *
	 * @return The flags.
	 */
	static type FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the flags to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     flags The flags.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, type flags);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the flags from a human readable string.
	 *
	 * @param[in,out] is    The output stream.
	 * @param[out]    flags The flags.
	 *
	 * @return The flags.
	 */
	static std::istream& FromStream(std::istream & is, type & flags);
};

} /* namespace cxxwin */


#endif /* H84A6DE64_2533_4A24_A206_17E1E2347A61 */
