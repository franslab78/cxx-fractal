/******************************************************************************/
/*! @copyright
 * Copyright 2020 Frans Labuschagne
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/*! @file
 *
 * @brief The resize type enumerate.
 *
 * This file contains a class that represents resize type enumerate.
 *
 * @author Frans Labuschagne
 * @date   2020-03-19
 */
/*----------------------------------------------------------------------------*/

#pragma once
#ifndef H36FC6C04_5B65_41D9_BD14_0E592E99B379
#define H36FC6C04_5B65_41D9_BD14_0E592E99B379

#include "windows_includes.hpp"

#include <cstdint>
#include <string>
#include <string_view>

namespace cxxwin
{

/*! @brief Resize types.
 *
 * The type of resize requested when the window receive message after its
 * size has changed.
 *
 * https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-size
 */
class ResizeTypes
{
public:
	enum value_t
	{
		/*! @brief The window has been restored.
		 *
		 * The window has been resized, but neither the Minimised nor
		 * Maximised value applies.
		 */
		Restore = SIZE_RESTORED,
		/*! @brief The window has been minimised.
		 *
		 * The window has been minimised. */
		Minimised = SIZE_MINIMIZED,
		/*! @brief The window has been maximised.
		 *
		 * The window has been maximised.
		 */
		Maximised = SIZE_MAXIMIZED,
		/*! @brief Popup show
		 *
		 * Message is sent to all pop-up windows when some other window has been
		 * restored to its former size.
		 */
		MaxShow = SIZE_MAXSHOW,
		/*! @brief Popup hide
		 *
		 * Message is sent to all pop-up windows when some other window is
		 * maximised. */
		MaxHide = SIZE_MAXHIDE
	};

	ResizeTypes() = default;
	constexpr ResizeTypes(value_t value) : value_(value) {};

	// Allow switch and comparisons.
	constexpr operator value_t() const { return value_; }

	explicit operator bool() = delete;        // Prevent usage: if(fruit)

	constexpr bool operator==(ResizeTypes a) const { return value_ == a.value_; }
	constexpr bool operator!=(ResizeTypes a) const { return value_ != a.value_; }

	constexpr bool operator==(value_t value) const { return value_ == value; }
	constexpr bool operator!=(value_t value) const { return value_ != value; }

	friend bool operator==(value_t value, ResizeTypes a) { return value == a.value_; }
	friend bool operator!=(value_t value, ResizeTypes a) { return value != a.value_; }

	/*! @brief Convert to human readable string.
	 *
	 * This operation shall convert the enumerate literal to a human readable 
	 * string.
	 *
	 * @param[in] value The enumerate literal
	 *
	 * @return The human readable enumerate literal string.
	 */
	static char const * ToString(ResizeTypes value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in] str The enumerate literal string
	 *
	 * @return The enumerate literal.
	 */
	static ResizeTypes FromString(const std::string_view str);

	/*! @brief Stream to output.
	 *
	 * This operation shall stream the enumerate literal to output.
	 *
	 * @param[in,out] os    The output stream.
	 * @param[in]     value The enumerate literal.
	 *
	 * @return The output stream.
	 */
	static std::ostream& ToStream(std::ostream & os, ResizeTypes value);

	/*! @brief Convert from human readable string.
	 *
	 * This operation shall convert the enumerate literal from a human readable 
	 * string.
	 *
	 * @param[in,out] is    The input stream.
	 * @param[out]    value The enumerate literal.
	 *
	 * @return The input string.
	 */
	static std::istream& FromStream(std::istream & is, ResizeTypes & value);

private:
	value_t value_ = Restore;
};

} /* namespace cxxwin */

#endif /* H36FC6C04_5B65_41D9_BD14_0E592E99B379 */
