# cxx-fractal

**cxx-fractal** is an experiment with fractals.

# Source Code

## Download

**cxx-fractal** is hosted in a [GIT](https://git-scm.com/) repository at [https://gitlab.com/franslab78/cxx-fractal](https://gitlab.com/franslab78/cxx-fractal).
The develop source code branch, including the submodules, can be cloned as follows:

```bash
md location\to\clone\into
cd location\to\clone\into
git clone --recurse-submodules --branch develop https://gitlab.com/franslab78/cxx-fractal.git
```

## Dependencies

**cxx-fractal** depends on the following that are included as submodules:
* [clipp](https://github.com/muellan/clipp)

#Build

## Development Environment

**cxx-fractal** requires the following minimum system requirements to develop:
* Personal computer capable of running Microsoft Windows 10 64-bit. Windows 10 64-bit minimum system requirements are:
  * 1 gigahertz (GHz) or faster 64-bit (x64) processor
  * 2 GB RAM
  * 100 GB available hard disk space
  * DirectX 9 graphics device with WDDM 1.0 or higher driver
* Microsoft Windows 10 64-bit

**cxx-fractal** uses [CMake](https://cmake.org/) to generate its build scripts.

The following compilers can be used to build **cxx-fractal**:
* [MinGW gnu c++](https://nuwen.net/mingw.html)
* [clang](http://clang.llvm.org/)
* [Microsoft Visual Studio](https://visualstudio.microsoft.com/vs/) Desktop development with C++

**cxx-fractal** recomends [ninja](https://ninja-build.org/) as build tool.

### MinGW (nuwen-distro)

A MinGW Distro for Windows is available at [https://nuwen.net/mingw.html](https://nuwen.net/mingw.html)
The current version of **cxx-fractal** used version 17.1 of nuwen MinGW distribution, which in turn is based on mingw-w64 7.0.0 containing gcc version 9.2.0.
Download the installer from the web-site and follow the instructions to install.
Add the path to were the distribution was installed to your system paths.

### MinGW-64

Mingw-w64 is an advancement of the original [mingw.org](https://mingw.org/) project, created to support the GCC compiler on Windows systems. It is available at [http://mingw-w64.org/](http://mingw-w64.org/)

### clang
Clang provides a language front-end and tooling infrastructure for languages in the C language family (C, C++, Objective C/C++, OpenCL, CUDA, and RenderScript) for the LLVM project. Both a GCC-compatible compiler driver (clang) and an MSVC-compatible compiler driver (clang-cl.exe) are provided. It is available at [http://clang.llvm.org/](http://clang.llvm.org/)
Download the installer from the web-site and follow the instructions to install.
Add the path to were the distribution was installed to your system paths.

### Microsoft Visual Studio

Microsoft Visual Studio is available at [https://visualstudio.microsoft.com/](https://visualstudio.microsoft.com/).
The current version of **cxx-fractal** used Visual Studio 2019.
Download the installer from the web-site and follow the instructions to install "Desktop development with C++".

## Build Procedure

**cxx-fractal** makes use of CMake to create build scripts or project files.

### Microsoft Visual Studio 2019

A Microsoft Visual Studio 1029 solution file can be created as follows:

* Select "Developer Command Prompt for VS 2019" from windows start menu
* Enter the following in the console:
```bash
cd location\to\clone\into\cxx-fractal
md _build\\msvs & cd md _build\\msvs
cmake ..\\..
```
* Open the solution file generated: "location\to\clone\into\cxx-fractal\_build\msvc\cxx-fractal.sln"

### MinGW (nuwen-distro)

**cxx-fractal** can be build using MinGW, CMake and Ninja as follows (note that both should be available from the system path):

```bash
cd location\to\clone\into\cxx-fractal
md _build\\mingw & cd md _build\\mingw
cmake -G Ninja ..\\..
ninja
```

### clang

**cxx-fractal** can be build using Clang, CMake and Ninja as follows (note that both should be available from the system path):

```bash
cd location\to\clone\into\cxx-fractal
md _build\\clang & cd md _build\\clang
cmake -G Ninja ..\\..
ninja
```

## License

Copyright 2020 Frans Labuschagne

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Notes

### vdk-signals Fix

vdk-sidnals requires a fix in the CMakeLists.txt file: change

```
target_sources(signals PRIVATE ${CMAKE_SOURCE_DIR}/src/signals.h
                               ${CMAKE_SOURCE_DIR}/src/signals.cpp)
target_include_directories(signals PUBLIC ${CMAKE_SOURCE_DIR}/src)
```

to

```
target_sources(signals PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/signals.h
                               ${CMAKE_CURRENT_SOURCE_DIR}/src/signals.cpp)
target_include_directories(signals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src)
```

also comment out the addition of demo and test directories:

```
add_subdirectory(tests)
add_subdirectory(demo)
```

to

```
#add_subdirectory(tests)
#add_subdirectory(demo)
```
